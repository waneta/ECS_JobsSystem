using System;
using Unity.Entities;

using Unity.Jobs;
namespace  Example_Normal_03
{

    // 定义一个ComponentData用于存储旋转速度
    [Serializable]
    public struct RotationSpeed : IComponentData
    {
        public float Value;
    }
    
    // ComponentDataProxy is for creating a MonoBehaviour representation of this component (for editor support).
    // [UnityEngine.DisallowMultipleComponent]
    // public class RotationSpeedProxy : ComponentDataProxy<RotationSpeed> { } 
}