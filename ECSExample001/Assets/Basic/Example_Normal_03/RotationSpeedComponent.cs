using System;
using Unity.Entities;
namespace  Example_Normal_03
{

    // ComponentDataWrapper用于将ComponentData添加到GameObject，
    // 这一步需要手动添加，将来Unity会自动化这步操作。
    public class RotationSpeedComponent : ComponentDataWrapper<RotationSpeed> {} 
}