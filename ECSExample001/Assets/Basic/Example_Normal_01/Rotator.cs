using UnityEngine;

namespace  Example_Normal_01
{
    class Rotator : MonoBehaviour
    {
        // 数据：可以在Inspector窗口中编辑的旋转速度值
        public float speed;
        
        // 行为：从component中读取速度值，然后修改Transform组件中的rotation
        void Update()
        {
            transform.rotation *= Quaternion.AngleAxis(Time.deltaTime * speed, Vector3.up);
        }
    }
}