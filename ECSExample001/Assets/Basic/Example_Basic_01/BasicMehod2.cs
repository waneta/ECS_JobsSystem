using UnityEngine;

namespace Example_Basic_01
{
    public class BasicMethod2:MonoBehaviour{

        private void Awake()
        {
            Debug.Log("111");
        }

        public void OnEnable()
        {
            Debug.Log("222");
        }


        private void Start()
        { 
            Debug.Log("444");
        }

    }
}
