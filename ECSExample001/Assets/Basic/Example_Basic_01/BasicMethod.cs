using UnityEngine;

namespace Example_Basic_01
{
    public class BasicMethod
    {
        //这里需要说下RuntimeInitializeOnLoadMethod 跟 MonoBehaviour 是没有关系的，
        //只要脚本里的方法是静态且带有该 属性，都会被调用
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Init()
        { 
            Debug.Log("000");
        }


        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void InitOver()
        {
            Debug.Log("333");
        }


    }
}
