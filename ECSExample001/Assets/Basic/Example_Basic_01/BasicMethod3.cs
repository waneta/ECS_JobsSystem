using UnityEngine;

namespace Example_Basic_01
{
    public class BasicMethod3:MonoBehaviour{

        private void Awake()
        {
            Debug.Log("===111");
        }

        public void OnEnable()
        {
            Debug.Log("===222");
        }


        private void Start()
        { 
            Debug.Log("===444");
        }
        
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Init()
        { 
            Debug.Log("===000");
        }


        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void InitOver()
        {
            Debug.Log("===333");
        }
    }
}
