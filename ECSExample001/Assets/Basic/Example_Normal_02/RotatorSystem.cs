
/*
1，ComponentSystem是怎么知道GameObject身上的Rotator和Transform的？

EntityManager需要事先知道那些对应的Entity，然后才能像上面的例子那样去遍历所有的Component。

ECS附带一种GameObjectEntity组件。 当 OnEnable时，GameObjectEntity会创建一个包含GameObject身上所有Component的Entity。
这样，整个GameObject和它的全部Component就都能被ComponentSystem遍历到了。

*/
using Unity.Entities;
using UnityEngine;

namespace Example_Normal_02
{
    // 行为：继承自ComponentSystem来处理旋转操作
    class RotatorSystem : ComponentSystem
    {
        struct Group
        {
            // 定义该ComponentSystem需要获取哪些components
            public Transform Transform;
            public Rotator   Rotator;
        }
        
        override protected void OnUpdate()
        {
            // 这里可以看第一个优化点：
            // 我们知道所有Rotator所经过的deltaTime是一样的，
            // 因此可以将deltaTime先保存至一个局部变量中供后续使用，
            // 这样避免了每次调用Time.deltaTime的开销。
            float deltaTime = Time.deltaTime;
            
            // ComponentSystem.GetEntities<Group>可以高效的遍历所有符合匹配条件的GameObject
            // 匹配条件：即包含Transform又包含Rotator组件（在上面struct Group中定义）
            foreach (var e in GetEntities<Group>())
            {
                e.Transform.rotation *= Quaternion.AngleAxis(e.Rotator.Speed * deltaTime, Vector3.up);
            }
        }
    }
}