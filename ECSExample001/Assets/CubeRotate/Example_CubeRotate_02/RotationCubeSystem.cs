


/*
然后我们再写一个System，作为逻辑的处理。首先我们要继承ComponentSystem，
然后重写OnUpdate，这里相当于MonoBehaviour的Update，我们在这里处理逻辑。
Group则是组件系统运行所需的ComponentData列表，可以随意命名，
我们可以在这里写很多不同的Group，作为处理逻辑的条件。
GetEntities<Group>()表示获取指定的实体，所有符合条件的实体都将被获取到
（这里符合条件的是所有带有RotationCubeComponent组件的实体都将被获取到）。

 */

using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
namespace Example_CubeRotate_02
{
    public class RotationCubeSystem : ComponentSystem
    {
        struct Group
        {
            public RotationCubeComponent  rotation;
            public Transform transform;
        }
    
        protected override void OnUpdate()
        {
            foreach (var enitites in GetEntities<Group>())
            {
                enitites.transform.Rotate(Vector3.up * enitites.rotation.speed * Time.deltaTime);
            }
        }
    }
}