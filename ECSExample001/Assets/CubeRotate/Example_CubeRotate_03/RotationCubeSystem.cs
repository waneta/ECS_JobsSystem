


/*
我们也可以用另外一种方式来写System。Inject表示注入数据，在Unity启动时会自动赋值合适的值


最后，我们创建一个Cube，附加上GameObjectEntity给他一个实体ID标识，
附加上RotationCubeComponent组件，我们把旋转速度调为100，再创建几个Cube并且附加上GameObjectEntity，
但不附加RotationCubeComponent组件，运行Unity后发现，带有RotationCubeComponent组件的实体会自转，
没有带RotationCubeComponent组件的实体不会自传。
--------------------- 
作者：XM肖牧 
来源：CSDN 
原文：https://blog.csdn.net/yye4520/article/details/82217247 
版权声明：本文为博主原创文章，转载请附上博文链接！

 */



using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
namespace Example_CubeRotate_03
{
    
    public class RotationCubeSystem : ComponentSystem
    {
        struct Group
        {
            public readonly int Length;
            public ComponentArray<RotationCubeComponent> rotationCubeComponents;
            //public RotationCubeComponent  rotation;
            //public Transform transform;
        }
    
        [Inject] Group group;
    
        protected override void OnUpdate()
        {
            //foreach (var enitites in GetEntities<Group>())
            //{
            //    enitites.transform.Rotate(Vector3.up * enitites.rotation.speed * Time.deltaTime);
            //}
            for (int i = 0; i < group.rotationCubeComponents.Length; i++)
            {
                group.rotationCubeComponents[i].transform.Rotate(Vector3.up * group.rotationCubeComponents[i].speed * Time.deltaTime);
            }
        }
    }


}