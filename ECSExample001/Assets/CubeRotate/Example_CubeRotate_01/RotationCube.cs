//传统的写法写一个Cube的自转。
using UnityEngine;

namespace Example_CubeRotate_01
{
    
    public class RotationCube : MonoBehaviour {
    
        public float speed;
    
        // Use this for initialization
        void Start () {
            
        }
        
        // Update is called once per frame
        void Update () {
            transform.Rotate(Vector3.up * speed * Time.deltaTime);
        }
    }

}