namespace Shooter.Classic
{
    public class GameManager{
        private static GameManager _instance;
        public static GameManager instance{
            get{
                if(_instance==null){
                    _instance = new GameManager();
                }
                return _instance;
            }
        }

        public float enemySpeed  = 10;
        public int bottomBound;
        public int topBound;
    }
}