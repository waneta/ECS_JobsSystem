using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
namespace Shooter.JobSystem
{
    public class GameManager : MonoBehaviour
    {

        // ...
        // GameManager classic members
        // ...

        TransformAccessArray transforms;
        MovementJob moveJob;
        JobHandle moveHandle;


        // ...
        // GameManager code
        // ...


        private static GameManager _instance;
        public static GameManager instance{
            get{
                if(_instance==null){
                    _instance = new GameManager();
                }
                return _instance;
            }
        }

        public float enemySpeed  = 10;
        public int bottomBound;
        public int topBound;
    }
}

