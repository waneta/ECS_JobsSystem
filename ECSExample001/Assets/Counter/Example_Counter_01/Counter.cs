﻿
/*
1、 没有使用 ECS的代码 ：
首先我会试着用MonoBehaviour来组织它。这是一个非常简单的代码。
编写完成后，您可以将Counter组件添加到适当的GameObject中。
 */
using UnityEngine;

namespace Example_Counter_01
{
	public class Counter : MonoBehaviour{    
		public  int  count;
		void Update () { 
			count++;
		}
	}
}