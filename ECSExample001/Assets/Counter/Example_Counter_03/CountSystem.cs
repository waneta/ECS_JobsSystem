﻿/*
在这段时间结束时，尝试并行化ECS处理。目标是增加计数。

当处理负载相当长或者有很多对象时，并行化可能是一个好结果。
 */
using Unity.Entities;
using Unity.Jobs;

namespace Example_Counter_03
{
	public class CountSystem : JobComponentSystem
	{
		AddCounterJob job;

		/*protected override void OnCreateManager(int capacity)
		{
			base.OnCreateManager(capacity);
			// 做一份工作
			job = new AddCounterJob();
		}*/

		protected override void OnCreateManager()
		{
			base.OnCreateManager();
			// 做一份工作
			job = new AddCounterJob();
		}

		// 发布工作
		/*protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			return job.Schedule(this, 64, inputDeps);
		}*/

		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			//return job.Schedule(this, 64, inputDeps);
			return job.Schedule(this, inputDeps);
		}

		//[ComputeJobOptimization] // Burst编译器的优化属性  //ComputeJobOptimizationAttribute.ComputeJobOptimizationAttribute()'“ComputeJobOptimizationAttribute”已过时:“Use Unity.Burst.BurstCompileAttribute instead
		[Unity.Burst.BurstCompile]
		struct AddCounterJob : IJobProcessComponentData<CountData> //请求ComponentData
		{
			public void Execute(ref CountData data)
			{
				data.count++;
			}
		}
	}
}