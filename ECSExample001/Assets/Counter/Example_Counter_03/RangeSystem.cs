/*
虽然它是一个系统，当它超出范围时删除实体......当然，它有必要获得“范围”。
因此，在(2) 中，RangeData包含在组中。

在(3)中，使用DestroyEntity删除实体。

这里值得注意的是Update ① 和  [ReadOnly] 。这两个用法大致相同，似乎如果[ReadOnly]存在，它将在使用[WriteOnly]等的系统之后被调用。
同样，如果有UpdateAfter，它将在指定的系统之后被调用。

 */
using Unity.Entities;
using Unity.Collections;
using Unity.Jobs;

namespace Example_Counter_03
{
    

    [UpdateAfter(typeof(CountSystem))]//1 add 
    public class RangeSystem : ComponentSystem
    {
        struct Group
        {
            public int Length;
            public EntityArray entities;
            [ReadOnly] public ComponentDataArray<CountData> countData; //2
            [ReadOnly] public SharedComponentDataArray<RangeData> rangeData;
        }

        [Inject] Group group;

        protected override void OnUpdate()
        {
            for (int i=0; i<group.Length; i++)
            {
                var range = group.rangeData[i];
                var data = group.countData[i];
                if ( data.count >  range.max || data.count < range.min )
                {
                    PostUpdateCommands.DestroyEntity(group.entities[i]);//3
                }
            }
        }
    }
}