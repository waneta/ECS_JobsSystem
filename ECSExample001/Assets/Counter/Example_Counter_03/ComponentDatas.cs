﻿using Unity.Entities;

namespace  Example_Counter_03
{

	// 实体
	public struct CountData : IComponentData
	{
		public int count;
	}
	/*
	首先，我们添加了RangeData，它是用于范围判断的ComponentData。
	但是，由于该“范围信息”在大多数数据中具有相同的值，因此使用对多个实体公用的IShardComponentData。
	还添加了Serializable属性，以便可以使用Inspector进行设置。
	*/
	[System.Serializable]
	public struct RangeData : ISharedComponentData
	{
		public int min, max;
	}
}