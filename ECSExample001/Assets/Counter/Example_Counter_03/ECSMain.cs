﻿/*
(1) 首先，RangeData显示在Inspector上，以便编辑，
(2) RangeData作为类型添加到原型中，
(3) RangeData设置在创建的Entity中。

 */
using UnityEngine;
using Unity.Entities;
namespace Example_Counter_03
{
	public class ECSMain : MonoBehaviour
	{

		[SerializeField] RangeData range;//1 add

    	EntityArchetype sampleArchetype;

		void Start()
		{
			var entityManager = World.Active.GetOrCreateManager<EntityManager>();
			sampleArchetype = entityManager.CreateArchetype(typeof(CountData), typeof(RangeData));//2 modify
		}

		private void Update()
		{
			if(Input.anyKey)
			{
				var entityManager = World.Active.GetOrCreateManager<EntityManager>();
				var entity = entityManager.CreateEntity(sampleArchetype);
				entityManager.SetSharedComponentData<RangeData>(entity, range);//3 add
			}
		}
	}
}

