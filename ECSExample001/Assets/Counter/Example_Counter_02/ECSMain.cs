﻿/*
 之后，如果您将ECSMain附加到适当的对象并Play，则第一步完成。
在Play期间，打开Window > EntityDebugger，当它从Systems列表中找到CountSystem时，它会变白，并且如果实体存在 。

如果没有实体，那么您有可能在没有CountData的情况下创建实体，或者您没有首先创建实体。另外，如果您没有系统，则创建ComponentSystem的代码有问题
 */
using UnityEngine;
using Unity.Entities;
namespace Example_Counter_02
{
	public class ECSMain : MonoBehaviour
	{
		void Start ()
		{
			// 获取EntityManager
			var entityManager = World.Active.GetOrCreateManager<EntityManager>();

			// 定义实体的原型
			var sampleArchetype = entityManager.CreateArchetype(typeof(CountData));

			// 实际上基于原型生成实体
			entityManager.CreateEntity(sampleArchetype);
		}
	}
}

