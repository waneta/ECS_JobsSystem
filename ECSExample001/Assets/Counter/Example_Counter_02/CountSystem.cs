﻿/*
接下来，让我们对应于ECS。有三件事要做
它是什么？麻烦？ ECS就是这样

CountData  		计数的值
CountSystem  	实际计数
ECSMain   		实体
 */
using Unity.Entities;

namespace  Example_Counter_02
{
	public class CountSystem : ComponentSystem
	{
		// System所需的ComponentData列表
		struct Group
		{
			public int Length;
			public ComponentDataArray<CountData> countData;
		}

		[Inject] Group group; // 注入请求的ComponentData

		// 调用每一帧
		protected override void OnUpdate()
		{
			for(int i=0; i<group.Length; i++)
			{
				var countData = group.countData[i];
				countData.count++;
				System.Console.WriteLine("countData.count: "+countData.count);
				group.countData[i] = countData;
			}
		}
	}
}