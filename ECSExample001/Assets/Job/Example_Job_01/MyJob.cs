using UnityEngine;


using Unity.Jobs;
using Unity.Collections;

using System.Threading.Tasks;
using System;
namespace  Example_Sample_01
{
    // Job adding two floating point values together
    public struct MyJob : IJob
    {
        public float a;
        public float b;
        public NativeArray<float> result;

        public void Execute()
        {
            result[0] = a + b;
        }

        // public async void Execute()
        // {
        //     await Task.Delay(TimeSpan.FromSeconds(1)); 
        //     result[0] = a + b;
           
        // }
    }

}