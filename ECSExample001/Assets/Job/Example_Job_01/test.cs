using UnityEngine;

using Unity.Collections;
using Unity.Jobs;

namespace  Example_Sample_01
{
    // Job adding two floating point values together
    public class test:MonoBehaviour
    {
        

        private void Start() {
            ScheduleJob();
        }
        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// This function can be called multiple times per frame (one call per event).
        /// </summary>
        void OnGUI()
        {
            if(GUILayout.Button("ScheduleJob")){
                ScheduleJob();
            }

            
        }


        void ScheduleJob(){
            // Create a native array of a single float to store the result. This example waits for the job to complete for illustration purposes
            NativeArray<float> result = new NativeArray<float>(1, Allocator.TempJob);

            // Set up the job data
            MyJob jobData = new MyJob();
            jobData.a = 10;
            jobData.b = 10;
            jobData.result = result;

            // Schedule the job
            JobHandle handle = jobData.Schedule();

            // Wait for the job to complete
            handle.Complete();

            // All copies of the NativeArray point to the same memory, you can access the result in "your" copy of the NativeArray
            float aPlusB = result[0];

            Debug.LogError("aPlusB: "+aPlusB);
            // Free the memory allocated by the result array
            result.Dispose();

        }

    }

}