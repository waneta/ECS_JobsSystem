using UnityEngine;


using Unity.Jobs;
using Unity.Collections;
namespace  Example_Sample_02
{
    // Job adding two floating point values together
    public struct MyJob : IJob
    {
        public float a;
        public float b;
        public NativeArray<float> result;


        public void Execute()
        {
            result[0] = a + b;
        }
    }

    // Job adding one to a value
    public struct AddOneJob : IJob
    {
        public NativeArray<float> result;
        
        public void Execute()
        {
            result[0] = result[0] + 1;
        }
    }

}