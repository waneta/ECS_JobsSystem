

using Unity.Jobs;
using Unity.Collections;

namespace  Example_Job_03
{
    struct IncrementByDeltaTimeJob: IJobParallelFor
    {
        public NativeArray<float> values;
        public float deltaTime;

        public void Execute (int index)
        {
            float temp = values[index];
            temp += deltaTime;
            values[index] = temp;
        }
    }
}