
using Unity.Jobs;
using Unity.Collections;

namespace Example_Job_03
{
    // Job adding two floating point values together
    public struct MyParallelJob : IJobParallelFor
    {
        [ReadOnly]
        public NativeArray<float> a;
        [ReadOnly]
        public NativeArray<float> b;
        public NativeArray<float> result;

        public void Execute(int i)
        {
            result[i] = a[i] + b[i];
        }
    }
}