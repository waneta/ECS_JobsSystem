﻿using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Rendering;
using Unity.Entities;
using static Unity.Mathematics.math;
using System;

public class Example11 : MonoBehaviour
{

    public struct RenderSprite : ISharedComponentData, IEquatable<RenderSprite>
    {
        public Sprite sprite;
        public bool Equals(RenderSprite other)
        {
            return sprite == other.sprite;
        }
        public override int GetHashCode()
        {
            int hash = 0;
            if (!ReferenceEquals(sprite, null)) hash ^= sprite.GetHashCode();
            return hash;
        }
    }
    public struct RenderSpriteECS : IComponentData
    {

        public Vector3 position; //显示坐标
        public Vector3 scale;    //显示缩放
        public Vector2 pivot;    //锚点区域
        public int index; //索引
    }
    public struct Example11Alias : IComponentData
    {
    }

    static public BatchRendererGroup m_BatchRendererGroup;
    private Mesh m_Mesh;
    private Material m_Material;
    private int m_BatchIndex = -1;
    private JobHandle m_Hadle;
  
    private List<Vector4> m_SpriteDataOffset = new List<Vector4>();

    public Sprite[] sprites;
    public Shader shader;
    public int initCount;
    
    void Start()
    {
        m_Mesh = Resources.GetBuiltinResource<Mesh>("Quad.fbx");
        m_Material = new Material(shader) { enableInstancing = true };
        m_Material.mainTexture = sprites[0].texture;

        for (int i = 0; i < initCount; i++)
        {
            Sprite sprite = sprites[i % sprites.Length];
            //添加图片
            AddEntitySprite(sprite, UnityEngine.Random.insideUnitSphere *15f, Vector3.one , 1f); //显示完整图片(整体缩小0.5呗)

        }

        Refresh();
    }


    void AddEntitySprite(Sprite sprite, Vector2 localPosition, Vector2 localScale ,float slider)
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var entity = entityManager.CreateEntity();
        float perunit = sprite.pixelsPerUnit;
        Vector3 scale = new Vector3((sprite.rect.width / perunit) * localScale.x, (sprite.rect.height / perunit) * localScale.y, 1f);
        Vector4 rect = GetSpreiteRect(sprite);
        scale.x *= slider;
        rect.x *= slider;

        RenderSpriteECS obj = new RenderSpriteECS();
        obj.position = localPosition;
        obj.pivot = new Vector2(sprite.pivot.x / perunit * localScale.x, sprite.pivot.y / perunit * localScale.y);
        obj.scale = scale;
        obj.index = m_SpriteDataOffset.Count;

        entityManager.AddSharedComponentData(entity, new RenderSprite { sprite = sprite });
        entityManager.AddComponentData(entity, obj);
        entityManager.AddComponentData(entity, new Example11Alias());

        m_SpriteDataOffset.Add(rect);
    }


    private void Refresh()
    {

        //1.参与渲染的sprite数量发生变化（增加、删除）需要重新m_BatchRendererGroup.AddBatch
        RefreshElement();
        //2.参与渲染的sprite数量没有发生变化，只是坐标发生变化，那么在Job里重新计算坐标
        //RefreshPosition();
        //3.参与渲染的sprite数量没有发生变化、坐标也没有发生变化，例如：血条 显示图片一部分，只需要重新刷新MaterialPropertyBlock即可。
        //RefreshBlock();
    }
    private void RefreshElement()
    {
        if (m_BatchRendererGroup == null)
        {
            m_BatchRendererGroup = new BatchRendererGroup(OnPerformCulling);
        }
        else
        {
            m_BatchRendererGroup.RemoveBatch(m_BatchIndex);
        }
        MaterialPropertyBlock block = new MaterialPropertyBlock();
        block.SetVectorArray("_Offset", m_SpriteDataOffset);
        m_BatchIndex = m_BatchRendererGroup.AddBatch(
          m_Mesh,
          0,
          m_Material,
          0,
          ShadowCastingMode.Off,
          false,
          false,
          default(Bounds),
          initCount,
          block,
          null);
    }

    void RefreshPosition()
    {
        m_Hadle.Complete();

        //使用Example11MoveSystem代替刷新坐标
        //m_Hadle = new UpdateMatrixJob
        //{
        //    Matrices = m_BatchRendererGroup.GetBatchMatrices(m_BatchIndex),
        //    objects = m_SpriteData,
        //}.Schedule(m_SpriteData.Length, 32);
    }

    public class Example11MoveSystem : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var matrices = Example11.m_BatchRendererGroup.GetBatchMatrices(0);
            var jobHandle = this.Entities
            .WithAll<Example11Alias>()
            .ForEach((ref RenderSpriteECS go) =>
            {
                var position = go.position;
                float x = position.x + (go.scale.x * 0.5f) - go.pivot.x;
                float y = position.y + (go.scale.y * 0.5f) - go.pivot.y;
                //根据RenderSpriteECS组件的坐标信息，修改BatchRendererGroup
                matrices[go.index] = Matrix4x4.TRS(float3(x, y, position.z),
                    Unity.Mathematics.quaternion.identity,
                    go.scale);
            })
            .WithChangeFilter<RenderSpriteECS>() //这里这里当RenderSpriteECS变化在进行更新
            .Schedule(inputDeps);
            jobHandle.Complete();
            return inputDeps;
        }
    }

    void RefreshBlock()
    {
        MaterialPropertyBlock block = new MaterialPropertyBlock();
        block.SetVectorArray("_Offset", m_SpriteDataOffset);
        m_BatchRendererGroup.SetInstancingData(m_BatchIndex, initCount, block);
    }

    public JobHandle OnPerformCulling(BatchRendererGroup rendererGroup, BatchCullingContext cullingContext)
    {
        //sprite不需要处理镜头裁切，所以这里直接完成job
        m_Hadle.Complete();
        return m_Hadle;
    }

    [BurstCompile]
    private struct UpdateMatrixJob : IJobParallelFor
    {
        public NativeArray<Matrix4x4> Matrices;
        [ReadOnly] public NativeList<RenderSpriteECS> objects;
        public void Execute(int index)
        {
            //通过锚点计算sprite实际的位置
            RenderSpriteECS go = objects[index];
            var position = go.position;
            float x = position.x + (go.scale.x * 0.5f) - go.pivot.x;
            float y = position.y + (go.scale.y * 0.5f) - go.pivot.y;

            Matrices[index] = Matrix4x4.TRS(float3(x, y, position.z),
                Unity.Mathematics.quaternion.identity,
                objects[index].scale);
        }
    }


    private Vector4 GetSpreiteRect(Sprite sprite)
    {
        var uvs = sprite.uv;
        Vector4 rect = new Vector4();
        rect[0] = uvs[1].x - uvs[0].x;
        rect[1] = uvs[0].y - uvs[2].y;
        rect[2] = uvs[2].x;
        rect[3] = uvs[2].y;
        return rect;
    }

    private void OnDestroy()
    {
        if (m_BatchRendererGroup != null)
        {
            m_BatchRendererGroup.Dispose();
            m_BatchRendererGroup = null;
        }
    }
}
