﻿using UnityEngine;
using Unity.Entities;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Collections;
using UnityEngine.Rendering;

public class Example9 : MonoBehaviour
{
    public Transform root;
    public Material m_Material;
    public Texture2D m_Lightmap;
    private MeshFilter m_MeshFilter;
    private List<Matrix4x4> m_Materix = new List<Matrix4x4>();
    private List<Vector4> m_LightmapOffset = new List<Vector4>();
    private MaterialPropertyBlock m_Block;

    private BatchRendererGroup m_BatchRendererGroup;
    void Start()
    {
        //获取节点下的所有MeshRenderer
        foreach (var item in root.GetComponentsInChildren<MeshRenderer>(true))
        {
            if (m_MeshFilter == null)
            {
                //取出第一个mesh，因为我们是做例子可以肯定保证下面的mesh都是一样的
                m_MeshFilter = item.GetComponent<MeshFilter>();
            }
            //保存每个物体的矩阵以及lightmapScaleOffset
            m_Materix.Add(item.localToWorldMatrix);
            m_LightmapOffset.Add(item.lightmapScaleOffset);

        }
        //隐藏所有节点
        root.gameObject.SetActive(false);
        //启动LIGHTMAP_ON宏
        m_Material.EnableKeyword("LIGHTMAP_ON");
        //为了避免GC所以只new一次MaterialPropertyBlock
        m_Block = new MaterialPropertyBlock();
        //设置具体用哪张烘焙贴图
        m_Block.SetTexture("unity_Lightmap", m_Lightmap);
        //将每个物体的lightmapScaleOffset传入shader中
        m_Block.SetVectorArray("_LightmapST", m_LightmapOffset.ToArray());

        m_BatchRendererGroup = new BatchRendererGroup(this.OnPerformCulling);
        int batchIndex = m_BatchRendererGroup.AddBatch(m_MeshFilter.mesh, 0, m_Material, 0, ShadowCastingMode.On, true, false, new Bounds(Vector3.zero, Vector3.one * 100f), m_Materix.Count, m_Block, null);
        var matrixes = m_BatchRendererGroup.GetBatchMatrices(batchIndex);
        //赋值
        for (int i = 0; i < matrixes.Length; i++)
        {
            matrixes[i] = m_Materix[i];
        }

    }

    JobHandle m_Handle;
    public JobHandle OnPerformCulling(BatchRendererGroup rendererGroup, BatchCullingContext cullingContext)
    {
        CullingJob culling = new CullingJob()
        {
            //因为只有一个批次，所以id是0，如果需要多次的话，那么这里要注意了
            matrixes = m_BatchRendererGroup.GetBatchMatrices(0),
            planes = Unity.Rendering.FrustumPlanes.BuildSOAPlanePackets(cullingContext.cullingPlanes, Allocator.TempJob),
            batchVisibility = cullingContext.batchVisibility,
            visibleIndices = cullingContext.visibleIndices,
        };
        var handle = culling.Schedule(m_Handle);
        m_Handle = JobHandle.CombineDependencies(handle, m_Handle);
        m_Handle.Complete();
        return handle;
    }



    [BurstCompile]
    public struct CullingJob : IJob
    {
        [ReadOnly] public NativeArray<Matrix4x4> matrixes;
        [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<Unity.Rendering.FrustumPlanes.PlanePacket4> planes;
        public NativeArray<BatchVisibility> batchVisibility;
        public NativeArray<int> visibleIndices;
        public void Execute()
        {
            //遍历这个批次每个元素
            for (var i = 0; i < batchVisibility.Length; i++)
            {
                var batchVisib = batchVisibility[i];
                int visibleInstancesIndex = 0;
                //因为立方体的是1米，所以Extents就是0.5，如果画多个不同Mesh的话这个值应该从外面传进来
                AABB localBond = new AABB() { Center = Vector3.zero, Extents = Vector3.one * 0.5f };
                for (int j = 0; j < matrixes.Length; j++)
                {
                    //计算每个AABB的世界坐标系的区域
                    var aabb = AABB.Transform(matrixes[j], localBond);
                    //判断每个元素是否在摄像机中
                    if (Unity.Rendering.FrustumPlanes.Intersect2(planes, aabb) != Unity.Rendering.FrustumPlanes.IntersectResult.Out)
                    {
                        //如果摄像机能看到设置正确的index
                        visibleIndices[visibleInstancesIndex] = batchVisib.offset + j;
                        //添加参与渲染元素的数量
                        visibleInstancesIndex++;
                    }
                }
                //最终计算出真正参与渲染的元素数量，以及不显示的元素
                batchVisib.visibleCount = visibleInstancesIndex;
                batchVisibility[i] = batchVisib;
            }
        }
    }
    private void OnDestroy()
    {
        if (m_BatchRendererGroup != null)
        {
            m_BatchRendererGroup.Dispose();
            m_BatchRendererGroup = null;
        }
    }

}
