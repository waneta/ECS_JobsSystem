﻿using UnityEngine;
using Unity.Entities;
using System.Collections.Generic;
using Unity.Scenes;

public class Example4 : MonoBehaviour
{
    [InternalBufferCapacity(8)]
    public struct DynamicData : IBufferElementData
    {
        public int Value;
    }

    public struct StaticData : IComponentData
    {
        public int Value;
    }

    private void Start()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        Entity entity = entityManager.CreateEntity(typeof(DynamicData), typeof(StaticData));
        DynamicBuffer<DynamicData> buffers = entityManager.AddBuffer<DynamicData>(entity);
        //根据情况添加0-8之间的动态数据
        for (int i = 0; i < 7; i++)
        {
            buffers.Add(new DynamicData() { Value = i });
        }
        //传统的静态数据，一个组件只能添加一个整形数据，无法形成数组
        entityManager.SetComponentData<StaticData>(entity, new StaticData() { Value = 0 });

        //获取数据的地方可能在另外一个类中，只要能拿到entity就可以遍历这段buffer
        DynamicBuffer<DynamicData> GetBuffers  =  entityManager.GetBuffer<DynamicData>(entity);
        foreach (DynamicData data in GetBuffers)
        {
            Debug.Log(data.Value);
        }

        foreach (int integer in GetBuffers.Reinterpret<int>())
        {
            Debug.Log(integer);
        }
    }

}
