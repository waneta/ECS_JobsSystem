﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
/* 5 Prefab转Entity对象
 * 只需要将上面的游戏对象保存Prefab就可以了，但是如果Prefab是以前传统的游戏对象的结构是否也能转成Entity实体对象呢？
 * 答案是“可以”。如下代码所示，我们做一个传统的Prefab来将他实例化Entity实体对象
 */
namespace myPractice2.Example05.Demo1
{
    

    public class Demo01 : MonoBehaviour
    {
        public GameObject prefab;
        /*
         * GameObjectConversionUtility.ConvertGameObjectHierarchy()方法将一个传统的Prefab转成Entity对象
         * 注意是转成一个全新的Entity对象并不是一段内存
         * 
         * Entity Debuger中看到 Entity 1
         * 
         * manager.Instantiate 通过之前转出来的Entity对象去实例化新的Entity对象。
         * Entity Debuger中看，右边的面板中可以切换每个Chunk块，显然新实例化出来的Entity和前面由Prefab转成对象没在一个Chunk中。
         */

        void Start()
        {
            var manager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
            //将prefab转成Entity
            var entityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
            //根据Entity实例化多个新的Entity

            for(int i = 0; i < 100; i++)
            {
                var intance = manager.Instantiate(entityPrefab);
#if UNITY_EDITOR//给Entity对象修改名字
                manager.SetName(intance, prefab.name + intance.Index);
#endif
                //随机一个坐标
                var position = transform.TransformPoint(UnityEngine.Random.onUnitSphere *3f);

            }

        }


    }
}
