﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;

//16 优化查询遍历速度
/*
 * 
 */
namespace myPractice2.Example16.Demo1_1
{

    public class Demo1_1 : MonoBehaviour
    {
        public GameObject prefab;
        public GameObject hero;

        public int initCount;
        float m_lastInitTime;

        EntityManager m_EntityManager;
        BlobAssetStore m_BlobAssetStore;
        Entity m_Entity;

        private void Start()
        {
            m_EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            m_BlobAssetStore = new BlobAssetStore();
            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, m_BlobAssetStore);
            m_Entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
            m_lastInitTime = Time.time;

            //--add--
            World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<RemoveSystem>().heroTransform = hero.transform;
            //--add end--

            World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MoveSystem>().heroTransform = hero.transform;
            


        }
        private void Update()
        {
            if (Time.time - m_lastInitTime > 1f)
            {
                m_lastInitTime = Time.time;
                for (int i = 0; i < initCount; i++)
                {
                    float3 pos = UnityEngine.Random.insideUnitSphere * 10f;
                    var instance = m_EntityManager.Instantiate(m_Entity);
                    m_EntityManager.SetComponentData<Translation>(instance, new Translation { Value = pos });
                    m_EntityManager.AddComponentData<Alias>(instance, new Alias());
                    //添加系统状态组件
                    m_EntityManager.AddComponentData<AliasSystem>(instance, new AliasSystem());

                }

            }
        }

        private void OnDestroy()
        {
            if (m_BlobAssetStore != null)
                m_BlobAssetStore.Dispose();
        }



    }

    public struct Alias : IComponentData
    {

    }


    public struct AliasSystem : ISystemStateComponentData { }//系统组件

    //------------------------ modify -------------------
    /*
     * WithChangeFilter<T>看似方便其实隐藏了很多细节。如果场景中实体对象非常多，并且实体组件也比较复杂，
     * 那么强烈建议大家使用IJobChumk,根据块来遍历。
     * 如下代码所示，通过chunk.DidChange就可以判断出这个块组件数组中某个数据是否发生变化。
     * 
     */
    //[AlwaysSynchronizeSystem] modify//这个是什么作用？
    public class RemoveSystem : JobComponentSystem
    {
        //外部负责传入中心灰色立方体的坐标
        public Transform heroTransform;
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

        private EntityQuery m_Query;

        protected override void OnCreate()
        {
            base.OnCreate();
            m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

            //需要注意的是myJob.Schedule(m_Query, inputDeps)；这段代码里的 m_Query，
            //因为通过IJobChunk遍历所以需要将刚进去的组件告诉它，如下代码所示，传入对应需要的组件即可
            m_Query = GetEntityQuery(typeof(Alias), typeof(Translation));
        }

        //--- add ---
        [BurstCompile]
        struct MyJob : IJobChunk
        {
            //传入坐标组件
            [ReadOnly]
            public ArchetypeChunkComponentType<Translation> translationType;

            //传入实体类型
            [ReadOnly]
            public ArchetypeChunkEntityType entityType;

            public float3 heroPos;

            //缓冲区用于删除组件
            public EntityCommandBuffer.Concurrent CommandBuffer;

            //系统的版本号
            public uint LastSystemVersion;


            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
            {
                //通过系统版本号和组件类型比较是否发生变化
                var translationChanged = chunk.DidChange(translationType, LastSystemVersion);
                //如果块中的Translation数组没有变化则不进行遍历
                if (!translationChanged) return;
                //取出坐标组件数组
                var translations = chunk.GetNativeArray(translationType);
                //取出坐标组件对应的实体数组
                var entities = chunk.GetNativeArray(entityType);

                for(int i = 0; i < translations.Length; i++)
                {
                    //当它们距离小于0.1米时删除
                    if(math.distance(translations[i].Value,heroPos) <= 0.1f)
                    {
                        //删除实体
                        CommandBuffer.DestroyEntity(chunkIndex, entities[i]);
                    }
                }

               
            }
        }
        //--- add ---
        //将IJobChunk块中的数据传给它
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {

            float3 heroPos = heroTransform.position;
            var myJob = new MyJob()
            {
                CommandBuffer = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent(),
                entityType = this.GetArchetypeChunkEntityType(),
                translationType = this.GetArchetypeChunkComponentType<Translation>(true),
                heroPos = heroPos,
                LastSystemVersion = this.LastSystemVersion
                
            };
            
            var jobHandle = myJob.Schedule(m_Query, inputDeps);

            m_EndSimulationEcbSystem.AddJobHandleForProducer(jobHandle);

            return jobHandle;
        }

        /*
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {

            float3 heroPos = heroTransform.position;
            float dt = Time.DeltaTime;
            float speed = 5f;

            var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent();

            var jobHandle = this.Entities
                .WithAll<Alias>()
                .WithChangeFilter<Translation>() //监听变化的组件
                .ForEach((Entity entity, int entityInQueryIndex, ref Translation translation, ref Rotation rotation) =>
                {
                    //当它们距离小于0.1米时加入缓冲区
                    if (math.distance(translation.Value, heroPos) <= 0.1f)
                    {
                        ecb.DestroyEntity(entityInQueryIndex, entity);
                    }

                })
                .WithoutBurst()
                .Schedule(inputDeps);

            //统一执行缓冲区的操作
            m_EndSimulationEcbSystem.AddJobHandleForProducer(jobHandle);
            return jobHandle;
        }*/
    }
    //-- modify end---


    public class MoveSystem : JobComponentSystem
    {
        public Transform heroTransform;
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {

            if (heroTransform != null)
            {
                float3 heroPos = heroTransform.position;
                float dt = Time.DeltaTime;
                float speed = 5f;

                var jobHandle = Entities.ForEach((ref Translation translation,ref Rotation rotation )=> {

                    float3 heading = heroPos - translation.Value;
                    rotation.Value = quaternion.LookRotation(heading, math.up());
                    translation.Value = translation.Value + math.forward(rotation.Value) * dt * speed;
                    
                
                }).Schedule(inputDeps);
                return jobHandle;
            }

            return inputDeps;
        }
    }


    public class EventMoveSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            //找到所有包含AliasSystem组件但是不包含Alias组件的实体
            Entities.WithAll<AliasSystem>().
                WithNone<Alias>().
                ForEach((Entity entity) =>
                {

                    //Debug.LogFormat("entity {0} 被删除！",entity.Index);//会导致掉帧
                    //删除最后的系统状态组件，系统会自动删除它的实体对象
                    PostUpdateCommands.RemoveComponent<AliasSystem>(entity);
                }

                );

        }
    }


}
