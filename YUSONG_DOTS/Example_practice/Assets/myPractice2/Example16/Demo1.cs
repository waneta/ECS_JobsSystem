﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;

//16 优化查询遍历速度
/*
 * ECS动辄几万个实体组件，如果在Update每帧都遍历，性能也无法最优化。就拿寻找目标来说，即使场景中有很多实体组件，
 * 但是并不一定没帧它们都有在移动。如果说我们只对坐标发生变化的组件进行更新，那么就能减少性能开销。
 * 如下代码所示，我们新写一个系统 RemoveSystem，专门负责删除组件
 * 
 */
namespace myPractice2.Example16.Demo1
{

    public class Demo1 : MonoBehaviour
    {
        public GameObject prefab;
        public GameObject hero;

        public int initCount;
        float m_lastInitTime;

        EntityManager m_EntityManager;
        BlobAssetStore m_BlobAssetStore;
        Entity m_Entity;

        private void Start()
        {
            m_EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            m_BlobAssetStore = new BlobAssetStore();
            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, m_BlobAssetStore);
            m_Entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
            m_lastInitTime = Time.time;

            //--add--
            World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<RemoveSystem>().heroTransform = hero.transform;
            //--add end--

            World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MoveSystem>().heroTransform = hero.transform;
            


        }
        private void Update()
        {
            if (Time.time - m_lastInitTime > 1f)
            {
                m_lastInitTime = Time.time;
                for (int i = 0; i < initCount; i++)
                {
                    float3 pos = UnityEngine.Random.insideUnitSphere * 10f;
                    var instance = m_EntityManager.Instantiate(m_Entity);
                    m_EntityManager.SetComponentData<Translation>(instance, new Translation { Value = pos });
                    m_EntityManager.AddComponentData<Alias>(instance, new Alias());
                    //添加系统状态组件
                    m_EntityManager.AddComponentData<AliasSystem>(instance, new AliasSystem());

                }

            }
        }

        private void OnDestroy()
        {
            if (m_BlobAssetStore != null)
                m_BlobAssetStore.Dispose();
        }



    }

    public struct Alias : IComponentData
    {

    }


    public struct AliasSystem : ISystemStateComponentData { }//系统组件

    //-- add---
    /*
     * 
     * 监听变化的组件很容易，WithChangeFilter<T>就可以了，但是细心些你会发现有些明明没有变化的组件也执行了。
     * 从原理上来讲如果想精准地监听每个组件是否变化，那么久必然要在开辟一块保存它的哈希值，这无疑就会造成内存的浪费。
     * 
     * 如图所示，在Chunk块中每个连续的组件数组整体共享一个哈希值，当这个数据标记成可读写时，
     * 每次操作它Version Number就会变化，从而WithChangeFilter<T> 就能知道组件是否变化了。
     * 
     * 举个例子，比如现在N个实体的位置组件都保存在一个Chunk中，即使在逻辑中只修改了其中一个实体的坐标信息，
     * 但是由于他们都是共享的Version Number,那么整个块的数据都会被WithChangeFilter<T>遍历出来。
     * 
     */
    [AlwaysSynchronizeSystem]
    public class RemoveSystem : JobComponentSystem
    {
        //外部负责传入中心灰色立方体的坐标
        public Transform heroTransform;
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {

            float3 heroPos = heroTransform.position;
            float dt = Time.DeltaTime;
            float speed = 5f;

            var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent();

            var jobHandle = this.Entities
                .WithAll<Alias>()
                .WithChangeFilter<Translation>() //监听变化的组件
                .ForEach((Entity entity, int entityInQueryIndex, ref Translation translation, ref Rotation rotation) =>
                {
                    //当它们距离小于0.1米时加入缓冲区
                    if (math.distance(translation.Value, heroPos) <= 0.1f)
                    {
                        ecb.DestroyEntity(entityInQueryIndex, entity);
                    }

                })
                .WithoutBurst()
                .Schedule(inputDeps);

            //统一执行缓冲区的操作
            m_EndSimulationEcbSystem.AddJobHandleForProducer(jobHandle);
            return jobHandle;
        }
    }
    //-- add end---


    /*
     * 
     */
    public class MoveSystem : JobComponentSystem
    {
        public Transform heroTransform;
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {

            if (heroTransform != null)
            {
                float3 heroPos = heroTransform.position;
                float dt = Time.DeltaTime;
                float speed = 5f;

                var jobHandle = Entities.ForEach((ref Translation translation,ref Rotation rotation )=> {

                    float3 heading = heroPos - translation.Value;
                    rotation.Value = quaternion.LookRotation(heading, math.up());
                    translation.Value = translation.Value + math.forward(rotation.Value) * dt * speed;
                    
                
                }).Schedule(inputDeps);
                return jobHandle;
            }

            return inputDeps;
        }
    }

    /*
     */
    public class EventMoveSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            //找到所有包含AliasSystem组件但是不包含Alias组件的实体
            Entities.WithAll<AliasSystem>().
                WithNone<Alias>().
                ForEach((Entity entity) =>
                {

                    //Debug.LogFormat("entity {0} 被删除！",entity.Index);//会导致掉帧
                    //删除最后的系统状态组件，系统会自动删除它的实体对象
                    PostUpdateCommands.RemoveComponent<AliasSystem>(entity);
                }

                );

        }
    }


}
