﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
/* 7 Entity支持烘焙贴图
 * 
 * 每个模型会根据UV1在自己的贴图中先采样像素，两个颜色加在一起就是最终的颜色
 * 了。这样虽然多一张贴图增加一次采样就能让效率飞升。
 * 
 * 还有一个尴尬的问题就是阴影，阴影可能并没有影响自己模型的颜色，而是投射在别的模型上，
 * 所以如果自己的模型修改了坐标阴影是不会跟它一起走的，不过我们只要保证场景不动就没有任何问题了。
 * 
 * 我们只要让每个模型拥有正确的UV2,就不愁使用烘焙贴图了。
 * 但是现在又遇到了一个问题，场景里面其实有很多模型是完全一样的。
 * 比如，我们放500个立方体，但是由于它们的位置不同，所以需要采样的烘焙贴图的UV也就不同，
 * 那么UV2肯定是不同的了，可以UV2作为Mesh的一部分，如果改了UV2就意味着这500个立方体在内存里就无法复用了
 * 一下变成了500份内存，这显然不能忍啊。
 * 
 * 所以Unity提供的方案是自动给每个相同的Mesh提供一套相同的UV2。
 * FBX导入Unty的时候有个Generate Lightmap UVs的选项，如果勾选就会自动生成UV2.
 * 
 * 
 */
namespace myPractice2.Example07.Demo1
{
    

    public class Demo01 : MonoBehaviour
    {

        public MeshRenderer meshRenderer;
        public Material m_Material;
        public Texture2D m_Lightmap;
        private MeshFilter m_MeshFilter;



        void Start()
        {
            meshRenderer.gameObject.SetActive(false);
            m_MeshFilter = meshRenderer.GetComponent<MeshFilter>();

            //将lightmap贴图和偏移传入
            m_Material.SetVector("_LightmapST", meshRenderer.lightmapScaleOffset);
            m_Material.SetTexture("unity_Lightmap", m_Lightmap);

        }
        private void Update()
        {
            //开始渲染
            Graphics.DrawMesh(m_MeshFilter.mesh, meshRenderer.transform.localToWorldMatrix, m_Material, 0);
        }

    }
}
