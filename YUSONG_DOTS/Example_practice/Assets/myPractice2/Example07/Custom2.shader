﻿Shader "Unlit/Custom1"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        //---add------
        _LightmapST("_LightmapST",Vector) = (0,0,0,0)
        //---add------
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            //#pragma multi_compile_fog

            //add
            #pragma multi_compile_instancing

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;

                //---add------
                float3 uv1:TEXCOORD1;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                //---add------
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                //---add------
                float2 uv1 : TEXCOORD1;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                //---add------

                //UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _LightmapST;
            //---add------
            //将MeshRenderer中的LightmapScaleOffset分别传入 _LightmapST中
            UNITY_INSTANCING_BUFFER_START(Props)
            UNITY_DEFINE_INSTANCED_PROP(fixed4,_LightmapST)
            UNITY_INSTANCING_BUFFER_START(Props)
            //---add------
            v2f vert (appdata v)
            {
                v2f o;

                //---add------
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v,o);

                //---add------


                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                //---add------
                //取出每一个物体的LightmapScaleOffset重新算UV2
                fixed4 _I = UNITY_ACCESS_INSTANCED_PROP(Props, _LightmapST);
                o.uv1 = v.uv1.xy * _I.xy + _I.zw;
                
                //UNITY_TRANSFER_FOG(o,o.vertex);
                //---add------
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);

                col.rgb = DecodeLightmap(UNITY_SAMPLE_TEX2D(unity_Lightmap, i.uv1.xy));
                // apply fog
                //UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
