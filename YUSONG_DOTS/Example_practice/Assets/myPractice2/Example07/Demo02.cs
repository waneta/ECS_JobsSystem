﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
/*  
 * 7.1demo 无法合并DrawCall,这是致命的问题，不过我们可以借助GPU Instancing功能，
 * 首先修改Shader让它支持GPU Instancing.
 * 
 */
namespace myPractice2.Example07.Demo2
{
    

    public class Demo02 : MonoBehaviour
    {

        public Transform root;
        public Material m_Material;
        public Texture2D m_Lightmap;
        private MeshFilter m_MeshFilter;

        private List<Matrix4x4> m_Materix = new List<Matrix4x4>();
        private List<Vector4> m_LightmapOffset = new List<Vector4>();
        private MaterialPropertyBlock m_Block;



        void Start()
        {
            //获取节点下的所有MeshRender
            foreach(var item in root.GetComponentsInChildren<MeshRenderer>(true))
            {
                if(m_MeshFilter == null)
                {
                    //取出第一个mesh,因为我们是做例子可以肯定保证下面的mesh都是一样的
                    m_MeshFilter = item.GetComponent<MeshFilter>();

                }
                //保存每个物体的矩阵以及lightmapScaleOffset
                m_Materix.Add(item.localToWorldMatrix);
                m_LightmapOffset.Add(item.lightmapScaleOffset);
            }
            //隐藏所有节点
            root.gameObject.SetActive(false);
            //启动LIGHTMAP_ON宏
            m_Material.EnableKeyword("LIGHTMAP_ON");
            //为了避免GC所以只new一次MaterialPropertyBlock
            m_Block = new MaterialPropertyBlock();
            //设置具体用哪张烘焙贴图
            m_Block.SetTexture("unity_Lightmap", m_Lightmap);
            //将每个物体的lightmapScaleOffset传入shader
            m_Block.SetVectorArray("_LightmapST", m_LightmapOffset.ToArray());

           

        }
        private void Update()
        {
            //开始渲染
            Graphics.DrawMeshInstanced(m_MeshFilter.mesh, 0, m_Material,m_Materix, m_Block);

        }

    }
}
