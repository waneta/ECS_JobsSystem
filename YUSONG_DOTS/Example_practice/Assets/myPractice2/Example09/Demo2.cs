﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Jobs;
using Unity.Burst;
using UnityEngine.Rendering;
using Unity.Collections;
using Unity.Mathematics;

/* 9.2 ECS优化头顶血条
 * 
 * 说到底我们还是要对GPU Instancing有个充分地学习，
 * 如果想巧用，那么就必须了解MaterialPropertyBlock。
 * GPU Instancing有个限制，就是必须满足相同Mesh和相同材质，
 * 我们已经解决了烘焙贴图的问题，所以应用场景还是挺丰富的。
 * 
 * 3D部分我觉得用于做一些草、石头、栅栏这种大量出现的物体，
 * 但是其实这些物体大多都并不需要移动，所以Job的优势就不明显。
 * 2D部分我觉得可以用于优化头顶血条、冒血数字这些，
 * 由于这些可能需要大量的移动坐标，那么配合Job就非常适合了，这篇文章我们就来做个例子
 * 
 * 参与冒血或者血条的数字我们需要一个图集，由于2D都是相同的片,只是缩放不一样。
 * 那么它天然满足了相同网格、相同材质的必要条件。
 * 但是每个片需要采样的贴图区域不一样，还有每个片参与渲染的大小不一样，
 * 这些都在Shader中完成。
 * 
 * 如下图所示，首先我们先借助Unity的SpriteAtlas构建一个图集，同一个图集只会占用一个DrawCall.
 * 
 * 接着写一个脚本，把需要参与ECS渲染的Sprite拖上去
 * 
 * 渲染的原理就是m_BatchRendererGroup.AddBatch
 * 
 */

namespace myPractice2.Example09.Demo2
{
    
    /*ECS负责坐标的修改
     * 1）参与渲染的sprite数量发生变化（增加、删除），需要重新m_BatchRendererGroup.AddBatch。
     * 2）参与渲染的sprite数量没有发生变化，只是坐标发生变化，那么在Job里重新计算坐标。
     * 3）参与渲染的sprite数量没有发生变化、坐标也没有发生变化，
     *    例如：血条显示图片一部分，只需要重新刷新MaterialPropertyBlock即可。
     * 
     */
    public struct ECS
    {
        public Vector3 position;//显示坐标
        public Vector3 scale;//显示缩放
        public Vector3 pivot;//锚点区域
    }

    [BurstCompile]
    public struct UpdateMatrixJob : IJobParallelFor
    {
        public NativeArray<Matrix4x4> matrices;

        [ReadOnly]
        public NativeList<ECS> objects;

        public void Execute(int index)
        {
            //通过锚点计算sprite实际的位置
            ECS go = objects[index];
            var position = go.position;

            float x = position.x + (go.scale.x * 0.5f) - go.pivot.x;
            float y = position.y + (go.scale.y * 0.5f) - go.pivot.y;

            matrices[index] = Matrix4x4.TRS(new float3(x, y, position.z),
                Unity.Mathematics.quaternion.identity,
                objects[index].scale
                );
        }
    }


    public class Demo2 : MonoBehaviour
    {

        private BatchRendererGroup m_BatchRendererGroup;
        private Mesh m_Mesh;
        private Material m_Material;
        private int m_BatchIndex = -1;
        private JobHandle m_Handle;

        private NativeList<ECS> m_SpriteData;
        private List<Vector4> m_SpriteDataOffset = new List<Vector4>();
        public Sprite sprite1;
        public Sprite sprite2;
        public Sprite sprite3;

        public Shader shader;

        void Start()
        {
            m_SpriteData = new NativeList<ECS>(100, Allocator.Persistent);
            m_Mesh = Resources.GetBuiltinResource<Mesh>("Quad.fbx");
            m_Material = new Material(shader) { enableInstancing = true };
            m_Material.mainTexture = sprite1.texture;

            //添加图片
            //显示图片一部分（横向0.5f)
            AddSprite(sprite1, Vector3.zero, Vector3.one, 0.5f);

            //显示完整图片（整体缩小0.5）
            AddSprite(sprite2, Vector3.zero + new Vector3(1,0,0), Vector3.one * 0.5f, 1f);

            //显示完整图片
            AddSprite(sprite3, Vector3.zero + new Vector3(1, 1, 0), new Vector3(10,2,1), 1f);

            Refresh();

        }



        void AddSprite(Sprite sprite,Vector2 localPosition,Vector2 localScale,float slider)
        {
            //官方文档：The number of pixels in the sprite that correspond to one unit in world space. (Read Only)
            //别人资料：在这张Sprite中，世界坐标中的一单位由几个Pixel组成
            //自己的理解：世界空间中，一米等于pixelsPerUnit， 例如sprite.pixelsPerUnit=100， 一米有100个像素
            float perunit = sprite.pixelsPerUnit;

            //【为什么要除以perunit？ 计算这张图片在世界空间显示的大小（物理），例如，sprite1.rect.width=395 sprite1.rect.width=395, 等于3.95=4.0，这张图片在世界空间显示4米x4米的大小 】
            Vector3 scale = new Vector3((sprite.rect.width/perunit)*localScale.x,(sprite.rect.height/perunit)*localScale.y,1f);
            Vector4 rect = GetSpriteRect(sprite);

            scale.x *= slider;
            rect.x *= slider;

            ECS obj = new ECS();
            obj.position = localPosition;
            obj.pivot = new Vector2(sprite.pivot.x / perunit * localScale.x, sprite.pivot.y / perunit * localScale.y);
            obj.scale = scale;

            m_SpriteData.Add(obj);
            m_SpriteDataOffset.Add(rect);

        }

        private void Refresh()
        {
            //1）参与渲染的sprite数量发生变化（增加、删除），需要重新m_BatchRendererGroup.AddBatch。
            RefreshElement();
            //2）参与渲染的sprite数量没有发生变化，只是坐标发生变化，那么在Job里重新计算坐标。
            RefreshPosition();
            //3）参与渲染的sprite数量没有发生变化、坐标也没有发生变化，
            //例如：血条显示图片一部分，只需要重新刷新MaterialPropertyBlock即可。
            //RefreshBlock();

        }

        private void RefreshElement()
        {
            if(m_BatchRendererGroup == null)
            {
                m_BatchRendererGroup = new BatchRendererGroup(OnPerformCulling);
            }

            MaterialPropertyBlock block = new MaterialPropertyBlock();
            block.SetVectorArray("_Offset", m_SpriteDataOffset);

            m_BatchIndex = m_BatchRendererGroup.AddBatch(
                m_Mesh,
                0,
                m_Material,
                0,
                ShadowCastingMode.Off,
                false,
                false,
                default(Bounds),
                m_SpriteData.Length,
                block,
                null

                ) ;
        }

        private void RefreshPosition()
        {
            m_Handle.Complete();
            m_Handle = new UpdateMatrixJob
            {
                matrices = m_BatchRendererGroup.GetBatchMatrices(m_BatchIndex),
                objects = m_SpriteData,

            }.Schedule(m_SpriteData.Length, 32);
        }
        
        private void RefreshBlock()
        {
            MaterialPropertyBlock block = new MaterialPropertyBlock();
            block.SetVectorArray("_Offset", m_SpriteDataOffset);
            m_BatchRendererGroup.SetInstancingData(m_BatchIndex, m_SpriteData.Length, block);
        }



        public JobHandle OnPerformCulling(BatchRendererGroup rendererGroup, BatchCullingContext cullingContext)
        {
            m_Handle.Complete();
            return m_Handle;
        }

        private Vector4 GetSpriteRect(Sprite sprite)
        {
            //【sprite.uv 是什么数据？ 应该是这种图片在图片集纹理的UV?】
            //官方文档：public Vector2[] uv; The base texture coordinates of the sprite mesh.
            //自己理解：坐标系(左上角,向右 X正轴 向下Y轴），uvs[0]是图片左上角在图集纹理uv坐标，uvs[1]图片右下角，uvs[2]图片中心在图集纹理uv坐标

            var uvs = sprite.uv;
            Vector4 rect = new Vector4();
            rect[0] = uvs[1].x - uvs[0].x;//【计算是干嘛的? 应该计算width?】
            rect[1] = uvs[0].y - uvs[2].y;//[计算height?】
            rect[2] = uvs[2].x;           //[计算x位置?】
            rect[3] = uvs[2].y;           //[计算y位置?】
            return rect;
        }
        private void OnDestroy()
        {
            if (m_BatchRendererGroup != null)
            {
                m_BatchRendererGroup.Dispose();
                m_BatchRendererGroup = null;
            }
            m_SpriteData.Dispose();
        }


    }
}

