﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Entities;
using UnityEngine.Profiling;

//9 RenderMesh渲染原理和ECS优化头顶血条
/*9.1  RenderMesh渲染原理
 * 
 * 现在的例子其实和Hybird的RenderMesh已经很接近了，还没有做的就是LOD功能，
 * 最后我们再来看看RenderMesh是如何实现的。实体的组件前面我们都已经介绍完毕，
 * RenderMesh是由RenderMeshSystemV2系统来驱动的。如下代码所示，它会在场景中找到
 * 静止不动的物体和会发生移动的物体，分别在多线程Job中更新它们。
 * 
 * 
 * 
 * 
 */

namespace myPractice2.Example09.Demo1
{
    public class Demo1 : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
    /*
     * 接着就是在InstancedRenderMeshBatchGroup类中处理裁切和LOD,
     * 由于代码比较多我就不引用了，大家后续也可以自行阅读学习。
     * 总之我们一定记住，所谓的ECS渲染就是在多线程中准备数据，最后再调用渲染API来绘制它。
     * 
    class RenderMeshSystemV2 : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            inputDeps.Complete();

            m_InstancedRenderMeshBatchGroup.CompleteJobs();
            m_InstancedRenderMeshBatchGroup.ResetLod();

            Profiler.BeginSample("UpdateFrozenRenderBatches"); ;
            UpdateFrozenRenderBatches();
            Profiler.EndSample();

            Profiler.BeginSample("UpdateDynamicRenderBatches"); ;
            UpdateDynamicRenderBatches();
            Profiler.EndSample();

            m_InstancedRenderMeshBatchGroup.LastUpdatedOrderVersion = EntityManager.GetComponentOrderVersion<RenderMesh>();
            
            return new JobHandle();
        }
    }*/
    
}

