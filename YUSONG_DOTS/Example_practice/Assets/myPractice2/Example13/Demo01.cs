﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Scenes;
/* 13 场景切换加载
* 
* ECS提供了将场景批量转换的功能。我们可以利用这个功能将场景进行分块，
* 根据角色行走的坐标来动态启动/卸载对应的块。示例中，我们最好保证每个块的顺序是从左到右，从下到上，
* 默认情况下取消勾选AutoLoadScene,这样启动游戏就不会加载这个块了。
* 
* 已知每个块的区域是正方形，并且宽高都是10x10米。此时我们假设角色在场景中漫步，使用角色的X轴坐标和Z轴坐标分别乘以10，
* 就能算出来角色所在块的水平垂直方向的Index。假设我们的地形地盘是4x4块，如下所示：
* [12, 13, 14, 15]  
* [8,  9,  10, 11]
* [4,  5,  6,  7]
* [0,  1,  2, 3]
* 
* 假设主角当前的XZ坐标是（21,11)
* 21/10 = 2
* 11/10 = 1
* 
* 从而计算出角色所在的水平垂直方向的Index(2,1)。场景中，6号块，
* 周边1，2,3,5,7,,9,10，11都是他的周围块需要马上加载出来。
* 
* 运行是游戏后直接在Scene视图中拖动Prefab的X和Z坐标场景中的地块就会动态加载或者卸载。
*/
namespace myPractice2.Example13.Demo1
{
    
    public class Demo01 : MonoBehaviour
    {
        SceneSystem m_SceneSystem;

        public Transform hero;
        public SubScene[] subScenes;
        private int m_LastX = -1, m_LastZ = -1;

        private Dictionary<int, SubScene> m_LastLoadScene = new Dictionary<int, SubScene>();
        private HashSet<int> m_CurrentLoadSceneIndex = new HashSet<int>();

        private int rowCount = 4;
        private int colCount = 4;
        void Start()
        {
            m_SceneSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<SceneSystem>();
        }

        // Update is called once per frame
        void Update()
        {
            int x = (int)(hero.position.x / 10f);
            int z = (int)(hero.position.z / 10f);
            bool isChange = (x != m_LastX) || (z != m_LastZ);

           

            if (isChange)
            {
                Debug.LogFormat("x:{0} y:{1}", x, z);
                m_LastX = x;
                m_LastZ = z;

                m_CurrentLoadSceneIndex.Clear();

                for(int col = x-1; col <= x+1; col++)
                {
                    for(int row = z-1; row <= z+1; row++)
                    {
                        if(row>=0 && col>=0 && row < rowCount && col < colCount)
                        {
                            int index = row * colCount + col;
                            if (index < subScenes.Length)
                            {
                                Debug.LogFormat("index: {0}", index);
                                m_LastLoadScene[index] = subScenes[index];
                                m_CurrentLoadSceneIndex.Add(index);

                                m_SceneSystem.LoadSceneAsync(subScenes[index].SceneGUID);
                            }
                        }
                    }
                }
                //卸载不再当前九宫格中的块
                List<int> keys = new List<int>(m_LastLoadScene.Keys);
                for(int i = 0;i < keys.Count; i++)
                {
                    var key = keys[i];
                    if (!m_CurrentLoadSceneIndex.Contains(key))
                    {
                        m_SceneSystem.UnloadScene(m_LastLoadScene[key].SceneGUID);
                        m_LastLoadScene.Remove(key);
                    }
                }
            }

        }
    }
}
