﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* 17.1 Entity烘培贴图例子
 * 三个塔，一地面
 * 使用了烘培，但是没有GPU Instancing
 * 
 * 
 * 
 * 
 * 
 */
namespace myPractice2.Example17.Demo1_2
{


    public class Demo1_2 : MonoBehaviour
    {
        public GameObject root;

       
        public Shader m_Shader;
        public Texture2D m_Lightmap;

        private Mesh m_Mesh;
        private Material[] m_MaterialArray;
        private Matrix4x4[] m_MatrixArray;
        void Start()
        {
            MeshRenderer[] _meshRendererArray =  root.GetComponentsInChildren<MeshRenderer>();

            m_MaterialArray = new Material[_meshRendererArray.Length];
            m_MatrixArray = new Matrix4x4[_meshRendererArray.Length];

            for (int i = 0; i < _meshRendererArray.Length; i++)
            {
                if (m_Mesh == null)
                {
                    m_Mesh = _meshRendererArray[i].GetComponent<MeshFilter>().mesh;
                }
                m_MatrixArray[i] = _meshRendererArray[i].transform.localToWorldMatrix;
                m_MaterialArray[i] = new Material(m_Shader);

                //将lightmap贴图和偏移传入
                m_MaterialArray[i].SetVector("_LightmapST", _meshRendererArray[i].lightmapScaleOffset);
                m_MaterialArray[i].SetTexture("unity_Lightmap", m_Lightmap);
            }

            root.gameObject.SetActive(false);
            
            



        }

        private void Update()
        {
            for(int i = 0;i< m_MaterialArray.Length; i++)
            {
                //开始渲染
                Graphics.DrawMesh(m_Mesh, m_MatrixArray[i], m_MaterialArray[i], 0);
            }
            
        }

    }
}
