﻿using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Rendering;
using static Unity.Mathematics.math;

/* 17.3 ECS绘制精灵
 * 本例是第9节《ECS优化头顶血条》的案例，同一个图集集中一次DrawCall绘制完毕。
 * 这个例子实现了完整渲染Sprite以及Sprite的一部分，
 * 例如血条这种根据血量显示其中的一部分，还可以设置Sprite的缩放，并且支持Sprite在Editor中设置的锚点对齐方式。
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
namespace myPractice2.Example17.Demo3
{
    public class Demo3 : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
