﻿Shader "myPractice02/Example17/Demo1-Run"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        //---add---
        _LightmapST("_LightmapST",Vector)=(0,0,0,0)
        //---add---
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            //---add---
            #pragma multi_compile_instancing
            //---add---
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                //---add---
                float3 uv1 : TEXCOORD1;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                //---add---
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                //---add---
                float2 uv1 : TEXCOORD1;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                //---add---
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            //将MeshRenderer中的LightmapScaleOffset分别传入_LightmapST中
            UNITY_INSTANCING_BUFFER_START(Props)
            UNITY_DEFINE_INSTANCED_PROP(fixed4, _LightmapST)
            UNITY_INSTANCING_BUFFER_END(Props)

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o); 
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                //---add---
                //取出每一个物体的LightmapScaleOffset重新算UV2
                fixed4 l =  UNITY_ACCESS_INSTANCED_PROP(Props, _LightmapST);
                o.uv1 = v.uv1.xy * l.xy + l.zw;
                //---add---
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                col.rgb = DecodeLightmap(UNITY_SAMPLE_TEX2D(unity_Lightmap, i.uv1.xy));
                return col;
            }
            ENDCG
        }
    }

}
