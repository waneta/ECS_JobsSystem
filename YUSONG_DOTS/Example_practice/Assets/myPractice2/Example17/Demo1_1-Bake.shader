﻿Shader "myPractice02/Example17/Demo1_1-Bake"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}

        _LightmapST("_LightmapST",Vector) = (0,0,0,0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            //*_*#pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;

                float2 uv1 : TEXCOORD1;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                //*_*UNITY_FOG_COORDS(1)
                float2 uv1 : TEXCOORD1;

                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _LightmapST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                //*_*UNITY_TRANSFER_FOG(o,o.vertex);

                //取出每个物体的LightmapScaleOffset重新算UV2
                o.uv1 = v.uv1.xy * _LightmapST.xy + _LightmapST.zw;

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                //*_*UNITY_APPLY_FOG(i.fogCoord, col);
                
                col.rgb *= DecodeLightmap(UNITY_SAMPLE_TEX2D(unity_Lightmap, i.uv1.xy));

                return col;
            }
            ENDCG
        }
    }
}
