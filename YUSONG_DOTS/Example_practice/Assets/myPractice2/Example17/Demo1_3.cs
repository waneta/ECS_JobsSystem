﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* 17.1 Entity烘培贴图例子
 * 三个塔，一地面
 * 使用了烘培，使用 GPU Instancing
 * 
 * Demo1_2中，有个问题，无法合并DrawCall ，这是致命的问题，
 * 首先需要修改Shader让它支持GPU Instancing, 查看Demo1_3-Run.shader
 * 
 * 
 * 
 * 
 * 
 */
namespace myPractice2.Example17.Demo1_3
{


    public class Demo1_3 : MonoBehaviour
    {
        public GameObject root;

       
        public Shader m_Shader;
        public Texture2D m_Lightmap;

        private Mesh m_Mesh;
        //private Material[] m_MaterialArray;
        private Material m_Material;

        private Matrix4x4[] m_MatrixArray;
        private Vector4[] m_LightmapOffsetArray;

        private MaterialPropertyBlock m_Block;
        void Start()
        {
            MeshRenderer[] _meshRendererArray =  root.GetComponentsInChildren<MeshRenderer>();

            //m_MaterialArray = new Material[_meshRendererArray.Length];
            m_MatrixArray = new Matrix4x4[_meshRendererArray.Length];
            m_LightmapOffsetArray = new Vector4[_meshRendererArray.Length];

            for (int i = 0; i < _meshRendererArray.Length; i++)
            {
                if (m_Mesh == null)
                {
                    m_Mesh = _meshRendererArray[i].GetComponent<MeshFilter>().mesh;
                }
                m_MatrixArray[i] = _meshRendererArray[i].transform.localToWorldMatrix;
                //m_MaterialArray[i] = new Material(m_Shader);

                //将lightmap贴图和偏移传入
                //m_MaterialArray[i].SetVector("_LightmapST", _meshRendererArray[i].lightmapScaleOffset);
                //m_MaterialArray[i].SetTexture("unity_Lightmap", m_Lightmap);
                m_LightmapOffsetArray[i] = _meshRendererArray[i].lightmapScaleOffset;
            }

            root.gameObject.SetActive(false);

            m_Material = new Material(m_Shader);

            //如果不设置材质的enableInstancing会报错
            //InvalidOperationException: Material needs to enable instancing for use with DrawMeshInstanced.
            m_Material.enableInstancing = true;

            m_Material.EnableKeyword("LIGHTMAP_ON");

            m_Block = new MaterialPropertyBlock();

            m_Block.SetTexture("unity_Lightmap", m_Lightmap);
            m_Block.SetVectorArray("_LightmapST", m_LightmapOffsetArray);

        }

        private void Update()
        {
            //for(int i = 0;i< m_MaterialArray.Length; i++)
            //{
            //    //开始渲染
            //    Graphics.DrawMesh(m_Mesh, m_MatrixArray[i], m_MaterialArray[i], 0);
            //}
            
           
            Graphics.DrawMeshInstanced(m_Mesh, 0, m_Material, m_MatrixArray, m_MatrixArray.Length, m_Block);
            
        }

    }
}
