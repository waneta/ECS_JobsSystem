﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* 17.1 Entity烘培贴图例子
 * 一个塔，一地面
 * 使用了烘培，但是没有GPU Instancing
 * 
 * 
 * 
 * 
 * 
 */
namespace myPractice2.Example17.Demo1_1
{


    public class Demo1_1 : MonoBehaviour
    {
        public MeshRenderer meshRenderer;
        public Material m_Material;
        public Texture2D m_Lightmap;
        private MeshFilter m_MeshFilter;

        void Start()
        {
            meshRenderer.gameObject.SetActive(false);
            m_MeshFilter = meshRenderer.GetComponent<MeshFilter>();
            //将lightmap贴图和偏移传入
            m_Material.SetVector("_LightmapST", meshRenderer.lightmapScaleOffset);
            m_Material.SetTexture("unity_Lightmap", m_Lightmap);
        }

        private void Update()
        {
            //开始渲染
            Graphics.DrawMesh(m_MeshFilter.mesh, meshRenderer.transform.localToWorldMatrix, m_Material, 0);
        }

    }
}
