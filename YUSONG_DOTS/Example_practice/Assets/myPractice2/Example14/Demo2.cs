﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;


/*14.2 寻找目标
* 
* 运行游戏后随机在场景中生成一些立方体实体，让它们都朝中间的灰色立方体聚集，
* 当距离比较接近的时候删掉实体。
* 
* 
*/
namespace myPractice2.Example14.Demo2
{
    
    public class Demo2:MonoBehaviour
    {
        public GameObject prefab;
        public GameObject hero;
        public int initCount;
        float m_lastInitTime;

        EntityManager m_EntityManager;
        BlobAssetStore m_BlobAssetStore;
        Entity m_Entity;

        private void Start()
        {
            m_EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            m_BlobAssetStore = new BlobAssetStore();
            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, m_BlobAssetStore);
            m_Entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
            m_lastInitTime = Time.time;

            World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MoveSystem>().heroTransform = hero.transform;

        }
        private void Update()
        {
            if(Time.time - m_lastInitTime > 1f)
            {
                m_lastInitTime = Time.time;
                for(int i = 0;i < initCount; i++)
                {
                    float3 pos = UnityEngine.Random.insideUnitSphere * 10f;
                    var instance = m_EntityManager.Instantiate(m_Entity);
                    m_EntityManager.SetComponentData<Translation>(instance, new Translation{ Value = pos });
                    m_EntityManager.AddComponentData<Alias>(instance, new Alias());
                    
                }

            }
        }

        private void OnDestroy()
        {
            if (m_BlobAssetStore != null)
                m_BlobAssetStore.Dispose();
        }



    }

    public struct Alias : IComponentData
    {

    }

    public class MoveSystem : JobComponentSystem
    {
        //外部负责传入中心灰色立方体的坐标
        public Transform heroTransform;
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            
            float3 heroPos = heroTransform.position;
            float dt = Time.DeltaTime;
            float speed = 5f;

            var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent();
            var jobHandle = this.Entities.WithAll<Alias>()
                .ForEach((Entity entity, int entityInQueryIndex, ref Translation translation, ref Rotation rotation) =>
                {
                    //每个符合条件的实体朝向中心灰色立方体移动
                    float3 heading = heroPos - translation.Value;
                    rotation.Value = quaternion.LookRotation(heading, math.up());
                    translation.Value = translation.Value + (dt * speed * math.forward(rotation.Value));

                    //当它们距离小于0.1米时加入缓冲区
                    if (math.distance(translation.Value, heroPos) <= 0.1f)
                    {
                        ecb.DestroyEntity(entityInQueryIndex, entity);

                        //还可以使用缓冲区创建实体，或者添加删除组件
                        //var e = ecb.CreateEntity(entityInQueryIndex);
                        //ecb.AddComponentData<Translation>(entityInQueryIndex, e);

                    }

                }).Schedule(inputDeps);
            //统一执行缓冲区的操作
            m_EndSimulationEcbSystem.AddJobHandleForProducer(jobHandle);
            return jobHandle;
        }
    }
}
