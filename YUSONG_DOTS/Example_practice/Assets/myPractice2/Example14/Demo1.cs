﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Entities;

/*14 动态缓冲区Dynamic Buffers和寻找目标
 * 14.动态缓冲区Dynamic Buffers
 * 
 * 在DOTS的原理部分中我们介绍过Dynamic Buffers可以保存数据数据，
 * 动态缓冲区是个非常重要的概念。IComponentData，只能保存值类型数据，如果保存一个数组就不行了。
 * 
 * 
 * 
 */
namespace myPractice.Example14.Demo1
{
    //传统实体组件只可能绑定一个int整形。但是我们这个实用了IBufferElementData
    //意思就是虽然实体对象绑定DynamicData,但是内部预留了8个整形数据的位置，代码中可以动态填充。
    [InternalBufferCapacity(8)]
    public struct DynamicData : IBufferElementData
    {
        public int Value;
    }

    public struct StaticData : IComponentData
    {
        public int Value;
    }

    public class Demo1 : MonoBehaviour
    {
        private void Start()
        {
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            Entity entity = entityManager.CreateEntity(typeof(DynamicData), typeof(StaticData));
            DynamicBuffer<DynamicData> buffers = entityManager.AddBuffer<DynamicData>(entity);

            //根据情况添加0 - 8之间的动态数据
            for(int i = 0; i < 7; i++)
            {
                buffers.Add(new DynamicData() { Value = i });
            }

            //传统的静态数据，一个组件只能添加一个整形数据，无法形成数组
            entityManager.SetComponentData<StaticData>(entity, new StaticData() { Value = 0 });

            //获取数据的地方可能在另个一个类中，只要能拿到entity就可以遍历这段buffer
            DynamicBuffer<DynamicData> GetBuffers = entityManager.GetBuffer<DynamicData>(entity);

            foreach(DynamicData data in GetBuffers)
            {
                Debug.Log("data.value:"+data.Value);
            }

            //可以调用Reinterpret强制将Buffers中的数据转成指定的数据，因为数据类型是int所以这里就是整形数组。
            foreach(int value in GetBuffers.Reinterpret<int>())
            {
                Debug.Log("Reinterpret: "+value);
            }

        }
    }
}
