﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//using UnityEngine;
//using Unity.Entities;
//using Unity.Jobs;
//using Unity.Transforms;
//using Unity.Mathematics;


///*14.2 寻找目标
//* 使用CommponentSystem系统
//* 
//* 
//* 
//*/
//namespace myPractice2.Example14.Demo2_1
//{
    
//    public class Demo2_1:MonoBehaviour
//    {
//        public GameObject prefab;
//        public GameObject hero;
//        public int initCount;
//        float m_lastInitTime;

//        EntityManager m_EntityManager;
//        BlobAssetStore m_BlobAssetStore;
//        Entity m_Entity;

//        private void Start()
//        {
//            m_EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
//            m_BlobAssetStore = new BlobAssetStore();
//            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, m_BlobAssetStore);
//            m_Entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
//            m_lastInitTime = Time.time;

//            World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MoveSystem>().heroTransform = hero.transform;

//        }
//        private void Update()
//        {
//            if(Time.time - m_lastInitTime > 1f)
//            {
//                m_lastInitTime = Time.time;
//                for(int i = 0;i < initCount; i++)
//                {
//                    float3 pos = UnityEngine.Random.insideUnitSphere * 10f;
//                    var instance = m_EntityManager.Instantiate(m_Entity);
//                    m_EntityManager.SetComponentData<Translation>(instance, new Translation{ Value = pos });
//                    m_EntityManager.AddComponentData<Alias>(instance, new Alias());
//                }

//            }
//        }



//    }

//    public struct Alias : IComponentData
//    {

//    }
//    /*
//     * 如果不需要Job多线程执行，而是使用ComponentSystem系统，那么代码就会在主线程中执行，
//     * 这样就可以使用 PostUpdateCommonds代码，变得更加简洁。
//     * 
//     * 注意：PostUpdateCommands同样也有创建实体、删除实体、添加组件、删除组件等常用方法。
//     */
//    public class MoveSystem : ComponentSystem
//    {
//        public Transform heroTransform;

//        protected override void OnUpdate()
//        {
//            //外部负责传入中心灰色立方体的坐标
//            float3 heroPos = heroTransform.position;
//            float dt = Time.DeltaTime;
//            float speed = 5f;

//            this.Entities.WithAll<Alias>()
//               .ForEach((Entity entity, ref Translation translation, ref Rotation rotation) =>
//               {
//                    //每个符合条件的实体朝向中心灰色立方体移动
//                    float3 heading = heroPos - translation.Value;
//                   rotation.Value = quaternion.LookRotation(heading, math.up());
//                   translation.Value = translation.Value + (dt * speed * math.forward(rotation.Value));

//                    //当它们距离小于0.1米时加入缓冲区
//                    if (math.distance(translation.Value, heroPos) <= 0.1f)
//                   {
//                       //添加到缓冲器，等待最后执行
//                       PostUpdateCommands.DestroyEntity(entity);
//                   }

//               });
            
//        }
//    }
//}
