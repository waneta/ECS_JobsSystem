﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using UnityEngine.Rendering;
/* 8 Entity 渲染原理
* 
* 如下代码所示，我们采用CommandBuffer来渲染GPU Instancing来渲染只有当物体坐标发生改变的时候才刷新
* 这样就能减少每帧带来的开销
* 
* 使用CommandBuffer.DrawMeshInstanced只有当元素位置改变时才刷新，这样效率明显高了很多
* 缺点SRP并不支持它，不过可以使用BatchRendererGroup
*
* Unity提供一个全新API BatchRendererGroup用于代替CommandBuffer.DrawMeshInstanced而且它对
* SRP Batcher的支持也更好。
*/
namespace myPractice2.Example08.Demo1
{
    public class Demo01_1 : MonoBehaviour
    {
        // Start is called before the first frame update

        
        public Material m_Material;//GPU Instancing材质
        public Mesh m_Mesh;//GPU Instancing网格

        public Transform target;
        public bool useCommandBuffer = false;//是否使用CommandBuffer渲染
        public Camera m_Camera;

        Matrix4x4[] m_matrix4x4s = new Matrix4x4[1023];
        void Start()
        {
            CommandBufferForDrawMeshInstanced();
        }


        private void OnGUI()
        {
            if (GUILayout.Button("当位置发生变化时候在更新"))
            {
                CommandBufferForDrawMeshInstanced();
            }
        }
        void Update()
        {
            if (!useCommandBuffer)
            {
                GraphicForDrawMeshInstanced();
            }
        }
        void SetPos()
        {
            for(int i= 0; i < m_matrix4x4s.Length; i++)
            {
                target.position = UnityEngine.Random.onUnitSphere * 10f;
                m_matrix4x4s[i] = target.localToWorldMatrix;
            }
        }
        void GraphicForDrawMeshInstanced()
        {
            if (!useCommandBuffer)
            {
                SetPos();
                Graphics.DrawMeshInstanced(m_Mesh, 0, m_Material, m_matrix4x4s, m_matrix4x4s.Length);
            }
        }
        CommandBuffer m_buffer = null;

        
        void CommandBufferForDrawMeshInstanced()
        {
            if (useCommandBuffer)
            {
                SetPos();
                if (m_buffer != null)
                {
                    m_Camera.RemoveCommandBuffer(CameraEvent.AfterForwardOpaque, m_buffer);
                    CommandBufferPool.Release(m_buffer);

                }
                m_buffer = CommandBufferPool.Get("DrawMeshInstanced");

                for(int i = 0; i < 1; i++)
                {
                    m_buffer.DrawMeshInstanced(m_Mesh, 0, m_Material, 0, m_matrix4x4s, m_matrix4x4s.Length);
                }
                m_Camera.AddCommandBuffer(CameraEvent.AfterForwardOpaque,m_buffer);
            }
        }
    }
}
