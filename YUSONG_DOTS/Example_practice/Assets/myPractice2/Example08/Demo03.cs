﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using UnityEngine.Rendering;
using Unity.Jobs;
/* 8 Entity 渲染原理
*  8.2 裁切问题
*  如下代码所示，BatchRendererGroup的构造函数就强制需要传入裁切Job方法，
*  每帧渲染前都会执行这个Job,然后就可以开始剔除了。
*  
*/
namespace myPractice2.Example08.Demo3
{
    


    public class Demo03 : MonoBehaviour
    {
        // Start is called before the first frame update

        //public MeshRenderer m_MeshRenderer;

        public Material m_Material; 
        public Mesh m_Mesh;
        //public Shader m_Shader;


        BatchRendererGroup m_BatchRendererGroup;

        void Start()
        {


            m_Mesh = Resources.GetBuiltinResource<Mesh>("Cube.fbx");
            //m_Material = new Material(m_Shader) { enableInstancing = true };
            //注意，材质，Shader需要支持GPU Instancing
            //系统自带Standard 材质默认是勾选
            m_Material.enableInstancing = true;

            m_BatchRendererGroup = new BatchRendererGroup(OnPerformCulling);
            int batchIndex = m_BatchRendererGroup.AddBatch(m_Mesh, 0, m_Material, 0,
                ShadowCastingMode.On, true, false,
                new Bounds(Vector3.zero, Vector3.one), 10, null, null
                );

            var matrixes = m_BatchRendererGroup.GetBatchMatrices(batchIndex);

            for(int i = 0;i< matrixes.Length; i++)
            {
                Matrix4x4 offset = Matrix4x4.identity;
                offset.m03 = i;
                matrixes[i] = offset;
            }

        }

        public JobHandle OnPerformCulling(BatchRendererGroup rendererGroup,BatchCullingContext cullingContext)
        {
            //遍历Batch,因为前面我们只加了一个Batch所以 i 只有 1 一个值
            for(int i = 0; i < cullingContext.batchVisibility.Length; i++)
            {
                //只有一个Batch对象，但是它绘制了10个元素
                BatchVisibility _batchVisibility = cullingContext.batchVisibility[i];
                int _index = 0;
                for(int j=0;j < _batchVisibility.instancesCount; j++)
                {
                    //当绘制2号元素的时候我们跳过，
                    //batchVisibility.offset表示索引的开始
                    if (j == 2) continue;
                    //没明白这个怎么用？？？
                    cullingContext.visibleIndices[_index] = _batchVisibility.offset + j;
                    _index++;
                }
                //这样参数显示的数量就变成9
                _batchVisibility.visibleCount = _index;
                //重新赋值batchVisibility对象
                cullingContext.batchVisibility[i] = _batchVisibility;
            }
            
            return default;
        }

        private void OnDestroy()
        {

            if (m_BatchRendererGroup != null)
            {
                m_BatchRendererGroup.Dispose();
                m_BatchRendererGroup = null;
            }
        }

    }
}
