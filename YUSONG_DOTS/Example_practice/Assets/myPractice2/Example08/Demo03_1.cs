﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using UnityEngine.Rendering;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
/* 8 Entity 渲染原理
*  8.2 裁切问题
*  
*  我们的目的就是将超过摄像机显示区域的物体剔除掉，从而提高渲染效率。
*  有了Bounds组件其实还不够，我们还需要根据摄像机方向、远近裁面以及每个物体的Bounds才能求得它是否在镜头中。
*  
*  我们开始改造之前的代码，加上镜头裁切并且将之前主线程的代码写在多线程Job中。
*  如下代码所示，注意new Bounds(Vector3.zero,Vector3.one * 10f),因为在一个Batcher中绘制了10个元素，
*  为了正确裁切，我们需要设置它们的总体渲染。由于每个立方体的长度是1，一个10个立方体这里区域就应该乘10.【还是不明白】
*  
*   【还是没搞明白Bounds用法】
*  
*/
namespace myPractice2.Example08.Demo3
{
    public class Demo03_1 : MonoBehaviour
    {

        public Material m_Material; 
        public Mesh m_Mesh;
        BatchRendererGroup m_BatchRendererGroup;

        void Start()
        {


            m_Mesh = Resources.GetBuiltinResource<Mesh>("Cube.fbx");
            //m_Material = new Material(m_Shader) { enableInstancing = true };
            //注意，材质，Shader需要支持GPU Instancing
            //系统自带Standard 材质默认是勾选
            m_Material.enableInstancing = true;

            
            m_BatchRendererGroup = new BatchRendererGroup(OnPerformCulling);
            //绘制10个元素，获取指定Batch的索引
            int batchIndex = m_BatchRendererGroup.AddBatch(m_Mesh, 0, m_Material, 0,
                ShadowCastingMode.On, true, false,
                new Bounds(Vector3.zero, Vector3.one * 10), 10, null, null
                );
            //
            var matrixes = m_BatchRendererGroup.GetBatchMatrices(batchIndex);

            /*
            for(int i = 0;i< matrixes.Length; i++)
            {
                Matrix4x4 offset = Matrix4x4.identity;
                offset.m03 = i;
                matrixes[i] = offset;
            }*/
            //多线程中添加数据，对比上面For理解
            AddJob add = new AddJob()
            {
                matrixes = matrixes
            };

            //等待多线程结束
            add.Run(matrixes.Length);

        }

        JobHandle m_Handle;

        public JobHandle OnPerformCulling(BatchRendererGroup rendererGroup,BatchCullingContext cullingContext)
        {
            CullingJob cullingJob = new CullingJob()
            {
                //因为只有一个批次，所以id是0，如果是多次的话，那么这里要注意了
                matrixes = m_BatchRendererGroup.GetBatchMatrices(0),
                //【cullingContext.cullingPlanes不太明白原理】
                planes = Unity.Rendering.FrustumPlanes.BuildSOAPlanePackets(cullingContext.cullingPlanes, Allocator.TempJob),
                batchVisibilityArray = cullingContext.batchVisibility,
                visibleIndices = cullingContext.visibleIndices,
            };

            var handle = cullingJob.Schedule(m_Handle);
            m_Handle = JobHandle.CombineDependencies(handle, m_Handle);
            m_Handle.Complete();
            return default;
        }

        private void OnDestroy()
        {

            if (m_BatchRendererGroup != null)
            {
                m_BatchRendererGroup.Dispose();
                m_BatchRendererGroup = null;
            }
        }

    }


    /*
     * 如下代码所示，下面就是负责添加的多线程任务。
     * 因为赋值操作的值就是索引，
     * 所有这里不需要考虑数据的先后顺序，这样就可以使用完全并行任务IJobParallelFor
     */

    [BurstCompile]
    public struct AddJob : IJobParallelFor
    {
        public NativeArray<Matrix4x4> matrixes;

        public void Execute(int index)
        {
            Matrix4x4 offset = Matrix4x4.identity;
            offset.m03 = index;
            matrixes[index] = offset;
        }
    }
    /*
     * (1) 为什么这里要用IJob而不是IJobParallelFor
     * 因为现在我们只添加了一个渲染批次，所以渲染这一个批次是对顺序有要求的。
     * 如果前面我们添加了多个渲染批次，那么这里就可以使用IJobParallerFor,
     * 可以让每个批次并行计算，但是每个批次内需要顺序执行
     * 
     * (2) FrustumPlanes.Intersect2(planes,aabb)
     * 请注意这个方法，它可以判断每个物体的AABB是否在摄像机显示区域中，可以返回物体和摄像机的关系，
     * 包括三个状态：
     * 1.物体完全在摄像机区域中
     * Unity.Rendering.FrustumPlanes.IntersectResult.In
     * 
     * 2.物体一部分在摄像机区域，而另一部分不在摄像机区域中
     * Unity.Rendering.FrustumPlanes.IntersectResult.Partial
     * 
     * 3.物体完全不在摄像机区域中，对应的结果就是
     * Unity.Rendering.FrustumPlanes.IntersectResult.Out
     * 
     * 
     */
    [BurstCompile]
    public struct CullingJob : IJob
    {
        [ReadOnly]
        public NativeArray<Matrix4x4> matrixes;

        [DeallocateOnJobCompletion]
        [ReadOnly]
        public NativeArray<Unity.Rendering.FrustumPlanes.PlanePacket4> planes;

        public NativeArray<BatchVisibility> batchVisibilityArray;

        public NativeArray<int> visibleIndices;


        public void Execute()
        {
            //遍历这个批次每个元素
            for(int i = 0; i < batchVisibilityArray.Length; i++)
            {
                BatchVisibility batchVisib = batchVisibilityArray[i];
                int _index = 0;
                //因为立方体是1米，所以Extents就是0.5f,如果画多个不同Mesh的话这个值应该从外面传进来
                AABB localBond = new AABB()
                {
                    Center = Vector3.zero,
                    Extents = Vector3.one * 0.5f
                };

                for(int j = 0;j < matrixes.Length; j++)
                {
                    //计算每个AABB的世界坐标系的区域
                    var aabb = AABB.Transform(matrixes[j], localBond);
                    //判断每个元素是否在摄像机中
                    if(Unity.Rendering.FrustumPlanes.Intersect2(planes,aabb)
                        != Unity.Rendering.FrustumPlanes.IntersectResult.Out)
                    {
                        //如果摄像机能看设置正确的index
                        visibleIndices[_index] = batchVisib.offset + j;
                        //参与渲染的数量
                        _index++;
                    }
                }
                //最终计算出真正参与渲染的元素数量，以及不显示的元素
                batchVisib.visibleCount = _index;
                batchVisibilityArray[i] = batchVisib;
            }
        }
    }

}
