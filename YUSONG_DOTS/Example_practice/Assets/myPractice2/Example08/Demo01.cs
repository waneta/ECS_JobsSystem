﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
/* 8 Entity 渲染原理
 * GPU Instancing和Entity并没有直接的关系，GPU InStancing也不依赖Entity,
 * 调用Graphics.DrawMeshInstance就可以进行渲染了。Entity只是一个实体对象，
 * 完全可以将一个立方体渲染绑定给这个实体，也可以把N个立方体绑定给实体。
 * 只要提供正确渲染的数据就可，渲染数据包括参与渲染的材质、网格、VBO、VAO和世界坐标矩阵。
 * 
 * 场景中静态的物体就比较简单，只需要初始化一次它们的数据即可。但是场景中还有动态的物体，这样就需要在它们运动前
 * 就要准备好新的渲染数据。如果是传统的方式调用Graphics.DrawMeshInstanced,我们实际上是在主线程来准备渲染数据，
 * 那么和ECS结合，将准备数据这块放在多线程JOB中，这样就能加快速度了。
 * 
 * Graphics.DrawMeshInstanced是一个比较底层的API ,虽然它也在主线程调用，但是实际的渲染是在多线中完成的，它也有好几个问题：
 * 1.不支持镜头裁切，也就是说如果一个物体并没有出现在视椎体中也会参与渲染占用DrawCall,所以裁切需要自己来做，可能是个比较大的工作量
 * 2.它不支持渲染超过1024个元素，如果需要超过，那么代码里就要自己控制，调用多次Graphic.DrawMeshInstanced。
 * 3.它需要在Update中每帧都调用，如果传统渲染管线可以使用CommandBuffer.DrawMeshInstanced，这样可以解决每帧都调用的问题，但是新版URP并不支持。
 * 4.建议使用CommandBuffer来渲染GPUInstancing，这样只要单物体位置或者属性发生变化，才进行强制刷新。
 * 
 */
namespace myPractice2.Example08.Demo1
{
    


    public class Demo01 : MonoBehaviour
    {
        // Start is called before the first frame update

        Matrix4x4[] m_matrix4x4s = new Matrix4x4[1023];
        Material m_material;
        Mesh m_Mesh;

        void Start()
        {

        }

        /*
         * 如果项目中使用GPU Instancing的话，很多人都会用底层API Graphics.DrawMeshInstanced在Update中绘制。
         * 先来做个极端测试 如下代码，
         * 
         * 在Profiler里 可以看到这个脚本update 耗时有27.68ms
         */
        void Update()
        {
            for(int i = 0;i < 1024; i++)
            {
                Graphics.DrawMeshInstanced(m_Mesh, 0, m_material, m_matrix4x4s, 1023);
            }
        }
    }
}
