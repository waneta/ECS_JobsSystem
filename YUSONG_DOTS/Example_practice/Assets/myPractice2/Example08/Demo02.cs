﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using UnityEngine.Rendering;
using Unity.Jobs;
/* 8 Entity 渲染原理
*  8.2 裁切问题
*/
namespace myPractice2.Example08.Demo2
{
    


    public class Demo02 : MonoBehaviour
    {
        // Start is called before the first frame update

        //public MeshRenderer m_MeshRenderer;

        public Material m_Material; 
        public Mesh m_Mesh;
        //public Shader m_Shader;


        BatchRendererGroup m_BatchRendererGroup;

        void Start()
        {


            m_Mesh = Resources.GetBuiltinResource<Mesh>("Cube.fbx");
            //m_Material = new Material(m_Shader) { enableInstancing = true };
            //注意，材质，Shader需要支持GPU Instancing
            //系统自带Standard 材质默认是勾选
            m_Material.enableInstancing = true;

            m_BatchRendererGroup = new BatchRendererGroup(OnPerformCulling);
            int batchIndex = m_BatchRendererGroup.AddBatch(m_Mesh, 0, m_Material, 0,
                ShadowCastingMode.On, true, false,
                new Bounds(Vector3.zero, Vector3.one), 10, null, null
                );

            var matrixes = m_BatchRendererGroup.GetBatchMatrices(batchIndex);

            for(int i = 0;i< matrixes.Length; i++)
            {
                Matrix4x4 offset = Matrix4x4.identity;
                offset.m03 = i;
                matrixes[i] = offset;
            }

        }

        public JobHandle OnPerformCulling(BatchRendererGroup rendererGroup,BatchCullingContext cullingContext)
        {
            return default;
        }

        private void OnDestroy()
        {

            if (m_BatchRendererGroup != null)
            {
                m_BatchRendererGroup.Dispose();
                m_BatchRendererGroup = null;
            }
        }

    }
}
