﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;

namespace myPractice2.Example02.Demo3
{


    /*
     * Entity Debugger就是用来查看Entity视图的窗口，选择GameObject实体后，
     * SpeedProxy序列化的数据已经被保存在右侧面板视图中了。另外，由于我们用了
     * ConvertToEntity将游戏对象转成Entity对象，所以实体还会自动添加LocalToWorld、Rotation、Translation组件
     * 出于效率的考虑，应当尽可能在运行是自己创建实体绑定脚本，这样才能做到最优化
     * 
     * 
     */
}
