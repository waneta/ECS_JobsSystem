﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;

namespace myPractice2.Example02.Demo1
{

    /*
     * 前面我们介绍组件IComponentData 是结构体，结构体是不能挂在游戏对象上的，
     * 所以我们需要再写一个代理类ComponentDataProxy这样就可以绑定给游戏对象了
     * 如下代码所示
     */

        
     [Serializable]//支持序列花
     public struct Speed : IComponentData
    {
        public int i;
        public float f;
        public double d;
    }

    /*将SpeedProxy类绑定给前面创建的游戏对象，就可以在面板中进行数据的序列化了。
     * 注意上面的红色报错就是提醒你这个类将被转换成Entity,不要对游戏对象本身有任何操作。
     */
    //添加代理类
    public class SpeedProxy : ComponentDataProxy<Speed> { }


}
