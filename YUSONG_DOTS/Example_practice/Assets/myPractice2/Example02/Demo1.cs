﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//2 实体与游戏对象
/* 
 * Hierarchy窗口创建一个空的GameObject,绑定ConvertToEntity
 * 
 * ConversionMode选择Convert And Destroy或者Convert And Inject GameObject
 * 前者表示Entity对象创建完后就自动销毁游戏对象，后者则表示保留游戏对象在Hierarchy中。
 * Entity的目的就是为了代替GameObject从而让自己轻量化，但是偶尔我们还需要在面板中进行编辑。
 * 
 */
namespace myPractice2.Example02.Demo1
{
    class Demo1
    {
    }
}
