﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;

using UnityEngine;

namespace myPractice2.Example02.Demo4
{


    /*
     * 由于我们用了IComponentData,所以只能序列化值类型数据，比如数组、字符串、贴图、材质等引用类型对象是无法序列化
     * 需要使用ISharedComponentData.
     */

    [Serializable]
    public struct Speed : ISharedComponentData, IEquatable<Speed>
    {
        public Texture2D texture;
        public bool Equals(Speed other)
        {
            return texture == other.texture;
        }
        public override int GetHashCode()
        {
            int hash = 0;
            if (!ReferenceEquals(texture, null)) hash ^= texture.GetHashCode();
            return hash;
        }
    }
    public class Demo04SpeedProxy : SharedComponentDataProxy<Speed> { }
}
