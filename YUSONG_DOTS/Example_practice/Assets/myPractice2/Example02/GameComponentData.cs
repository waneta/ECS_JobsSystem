﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;

namespace myPractice2.Example02.Demo3
{

    //DOTS的新版本添加了一个新关键字[GenerateAuthoringComponent]
    //这样不用谢上面的ComponentDataProxy代理类也可以将实体组体挂在游戏对象上

    [GenerateAuthoringComponent]
    public struct GameComponentData : IComponentData
    {
        public int x;
    }
}
