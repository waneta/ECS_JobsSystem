﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;

//15 监听删除的实体和组件
/*
 * ECS大部分情况都是在遍历中统一处理所有感兴趣的组件，这就好比for循环中进行删除，
 * 循环中只能难道当前需要删除的元素。如果需要在循环结束拿到所有被删除的元素，就得在循环中提前记录下删除的元素才行。
 * ECS同样也面临这个问题，不过它提供了ISystemStateComponentData组件。
 * 
 * 还是上一篇寻找目标的例子，如下代码所示，创建实体时我们在绑定一个系统状态组件给他，
 * 在运行代码可以发现即使调用了DestroyEntity方法，实体也并没有被删除。
 * 
 * 原因是系统状态组件是一个特殊组件，DestroyEntity会检测实体身上是否有这样的特殊组件，
 * 如果没有才删除自身和自身绑定的所有组件。如果实体身上有系统状态组件，则保留实体并且删除所有非系统状态的组件。
 * 
 * 
 * 
 */
namespace myPractice2.Example15.Demo1
{

    public class Demo1 : MonoBehaviour
    {
        public GameObject prefab;
        public GameObject hero;

        public int initCount;
        float m_lastInitTime;

        EntityManager m_EntityManager;
        BlobAssetStore m_BlobAssetStore;
        Entity m_Entity;

        private void Start()
        {
            m_EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            m_BlobAssetStore = new BlobAssetStore();
            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, m_BlobAssetStore);
            m_Entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
            m_lastInitTime = Time.time;

            World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MoveSystem>().heroTransform = hero.transform;

        }
        private void Update()
        {
            if (Time.time - m_lastInitTime > 1f)
            {
                m_lastInitTime = Time.time;
                for (int i = 0; i < initCount; i++)
                {
                    float3 pos = UnityEngine.Random.insideUnitSphere * 10f;
                    var instance = m_EntityManager.Instantiate(m_Entity);
                    m_EntityManager.SetComponentData<Translation>(instance, new Translation { Value = pos });
                    m_EntityManager.AddComponentData<Alias>(instance, new Alias());
                    //添加系统状态组件
                    m_EntityManager.AddComponentData<AliasSystem>(instance, new AliasSystem());

                }

            }
        }

        private void OnDestroy()
        {
            if (m_BlobAssetStore != null)
                m_BlobAssetStore.Dispose();
        }



    }

    public struct Alias : IComponentData
    {

    }


    public struct AliasSystem : ISystemStateComponentData { }//系统组件

    public class MoveSystem : JobComponentSystem
    {
        //外部负责传入中心灰色立方体的坐标
        public Transform heroTransform;
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {

            float3 heroPos = heroTransform.position;
            float dt = Time.DeltaTime;
            float speed = 5f;

            var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent();
            var jobHandle = this.Entities.WithAll<Alias>()
                .ForEach((Entity entity, int entityInQueryIndex, ref Translation translation, ref Rotation rotation) =>
                {
                    //每个符合条件的实体朝向中心灰色立方体移动
                    float3 heading = heroPos - translation.Value;
                    rotation.Value = quaternion.LookRotation(heading, math.up());
                    translation.Value = translation.Value + (dt * speed * math.forward(rotation.Value));

                    //当它们距离小于0.1米时加入缓冲区
                    if (math.distance(translation.Value, heroPos) <= 0.1f)
                    {
                        ecb.DestroyEntity(entityInQueryIndex, entity);

                        //还可以使用缓冲区创建实体，或者添加删除组件
                        //var e = ecb.CreateEntity(entityInQueryIndex);
                        //ecb.AddComponentData<Translation>(entityInQueryIndex, e);

                    }

                }).Schedule(inputDeps);
            //统一执行缓冲区的操作
            m_EndSimulationEcbSystem.AddJobHandleForProducer(jobHandle);
            return jobHandle;
        }
    }

    /*
     * 我们在另一个系统中监听实体是否被删除，如下代码，此时找到只包含系统状态组件的实体，
     * 最后调用PostUpdateCommands.RemoveComponet删除系统状态组件会自动将实体对象也删除。
     *
     * 当你删除一个绑定系统状态组件（SystemStateComponentData,简称SSC）的实体时，此时SSC是删除不掉的
     * 它对应的实体也没有真正被删除掉，只是把除了SSC以外的组件都删掉了而已，
     * 所以只要遍历只包含SSC组件的实体就能监听具体删除的哪个实体。
     * 
     * 如果想监听某个实体被创建的消息，在创建实体时只需要同时绑定系统状态组件（SSC），
     * 遍历只包含SSC组件的实体就能知道它被创建的事件【疑问，需要改例子测试】
     */
    public class EventMoveSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            //找到所有包含AliasSystem组件但是不包含Alias组件的实体
            Entities.WithAll<AliasSystem>().
                WithNone<Alias>().
                ForEach((Entity entity) =>
                {

                    //Debug.LogFormat("entity {0} 被删除！",entity.Index);//会导致掉帧
                    //删除最后的系统状态组件，系统会自动删除它的实体对象
                    PostUpdateCommands.RemoveComponent<AliasSystem>(entity);
                }

                );

        }
    }
}
