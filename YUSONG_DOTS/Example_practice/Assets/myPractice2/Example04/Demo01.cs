﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
/* 4 实体的坐标
 * 剖析它的内部实现原理 https://edu.uwa4d.com/lesson-detail/161/825/0?isPreview=0
 * 游戏对象转成的实体对象会自动绑定LocalToWorld组件,通过前面的旋转、缩放、平移代理组件将模型从自身的本地坐标系转到
 * 世界坐标系的4x4矩阵。如果你对这个矩阵的计算比较清楚，也可以只绑定LocalToWorld代理组件，并不需要在绑定旋转、缩放、平移代理组件
 * 
 * 可以详细推理下公式旋转矩阵，任务
 * 
 * Unity的旋转矩阵相乘的顺序必须满足Y*X*Z，只需要将各自轴的角度带进去就可以得到结果了
 * Matrix4x4 旋转矩阵 = Y轴旋转矩阵 * X旋转矩阵 * Z旋转矩阵
 * Matrix4x4 最终矩阵 = 位置矩阵 * 旋转矩阵 * 缩放矩阵
 * 
 * 矩阵来计算旋转还有个问题就是万向节死锁问题。
 * 
 */
namespace myPractice2.Example04.Demo1
{
    [Serializable]
    public struct Speed3 : ISharedComponentData, IEquatable<Speed3>
    {
        public Material material;
        public Mesh mesh;
        public Vector3 position;
        public bool Equals(Speed3 other)
        {
            return material == other.material && mesh == other.mesh;//&&不知是不是加 position == other.position
        }

        public override int GetHashCode()
        {
            int hash = 0;
            if (!ReferenceEquals(mesh, null)) hash ^= mesh.GetHashCode();
            if (!ReferenceEquals(material, null)) hash ^= material.GetHashCode();
            return hash;
        }

    }

    public class Demo01 : MonoBehaviour
    {
        // Start is called before the first frame update

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
