﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
/*3.2 实体网格材质
 * 利用Hybrid Renderer我们就可以很方便地添加实体网格材质，如下图所示，给一个空的游戏对象绑定RendererMeshProxy组件
 * 系统会添加GameObjectEntity组件，将网格和材质拖上去以后模型就显示了。
 * 
 * 此时你会发现一个问题，修改Transform下的位置、旋转、缩放没有用，继续绑定代理脚本Translation
 * 在Value处就可以修改模型的坐标了。
 * 
 * 如果需要旋转可以绑定代理脚本Rotation。Value中可以设置XYZW,注意它并不是欧拉角，需要填入的是四元数，也就是平常Transform.rotation的值
 * 已知XYZ轴的欧拉角是30°，60°，43°,如何算出四元数呢？
 * Unity.Mathematics.quaternion.EulerZXY(30f*Mathf.Deg2Rad,60f*Mathf.Deg2Rad,43f*Mathf.Deg2Rad);
 *  或者
 *   Quaternion.Euler(30f, 60f, 43f);
 *  四元数的公式如下：
 *  Q = cos(a/2) + i(x * sin(a/2)) + j(y * sin(a/2)) + k(z * sin(a/2))
 *  
 *  如果还需要缩放，可以绑定代理脚本NonUniformsScaleProxy，可以设置value的XYZ表示每个轴向的缩放比例
 *  有了这些代理脚本，我们就真正做到了按需设置，彻底把之前庞大的Transform组件从游戏对象中分解出来了。
 */
namespace myPractice2.Example03.Demo3
{
    /*
    public static quaternion EulerZXY(float3 xyz)
    {
        float3 s, c;
        sincos(0.5f * xyz, out s, out c);
        return quaternion(
            // s.x * c.y * c.z + s.y * s.z * c.x,
            // s.y * c.x * c.z - s.x * s.z * c.y,
            // s.z * c.x * c.y - s.x * s.y * c.z,
            // c.x * c.y * c.z + s.y * s.z * s.x,
            float4((s.xyz, c.x) * c.yxxy * c.zzyz + s.yxxy * s.zzyz * float4(c.xyz, s.x) * float4(1.0f, -1.0f, -1.0f, 1.0f);
            );
    }*/

    public class Demo03 : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            Unity.Mathematics.quaternion.EulerZXY(30f*Mathf.Deg2Rad,60f*Mathf.Deg2Rad,43f*Mathf.Deg2Rad);
            Quaternion.Euler(30f, 60f, 43f);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
