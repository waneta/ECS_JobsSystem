﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
/* 
 * 3 共享组件的隐患和实体网格材质
 * 3.1共享组件的隐患
 *   共享组件如果没有用好就会带来隐患，如下代码，这里有个共享组件，材质和网格属于引用，但是Vector3属于值类型
 */
namespace myPractice2.Example03.Demo1
{
    [Serializable]
    public struct Speed3 : ISharedComponentData,IEquatable<Speed3>
    {
        public Material material;
        public Mesh mesh;
        public Vector3 position;
        public bool Equals(Speed3 other)
        {
            return mesh == other.mesh && material == other.material;
        }

        public override int GetHashCode()
        {
            int hash = 0;
            if (!ReferenceEquals(mesh, null)) hash ^= mesh.GetHashCode();
            if (!ReferenceEquals(material, null)) hash ^= material.GetHashCode();
            return hash;
        }

    }

    public class Demo01 : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
