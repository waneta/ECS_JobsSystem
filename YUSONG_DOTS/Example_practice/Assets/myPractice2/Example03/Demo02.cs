﻿
using UnityEngine;
using Unity.Entities;
using System;

namespace myPractice2.Example03.Demo2
{
    /* 3.1共享组件的隐患
     * 场景中可能同时存在若干模型，它们的网络和材质可能是一样的，但是它们的坐标是不同的。
     * 按照demo1的代码，由于坐标不同导致大量的组件并不相同，所以无法得到真正的共享复用。
     * 我们应该把position从ISharedComponentData组件中拿出来。
     * 如下代码，在创建一个新的IComponentData组件，将position放进去。
     * 
     */

    [Serializable]
    public struct Speed3 : ISharedComponentData,IEquatable<Speed3>
    {
        public Material material;
        public Mesh mesh;
        public Vector3 position;

        public bool Equals(Speed3 other)
        {
            return mesh == other.mesh && material == other.material;
        }
        public override int GetHashCode()
        {
            int hash = 0;
            if (!ReferenceEquals(mesh, null)) hash ^= mesh.GetHashCode();
            if (!ReferenceEquals(material, null)) hash ^= material.GetHashCode();
            return hash;
        }
    }

    [Serializable]
    public struct Speed4 : IComponentData
    {
        public Vector3 position;

    }
    //这样我们将Speed3和Speed4同时绑定在实体组件中，就可以增大共享组件的复用力度了。

    public class Demo02 : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
