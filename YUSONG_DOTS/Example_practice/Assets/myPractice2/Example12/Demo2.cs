﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;

/* 12.2 对比ECS和非ECS的更新效率
 * 
 * 传统的开发中，更新游戏对象通常我们都尽可能地不挂脚本，而是在一个统一的地方对所以游戏对象进行更新。
 * 但是Unity提供的例子都是给每个对象挂个脚本来更新，我感觉这样做对比显然不太准备
 * 
 * 所以这篇例子我们来对比试验一个Update更新所有和使用Job来更新所有的性能差距。
 * 通过勾选UseECS来切换是否ECS来更新
 * 
 * 相同的逻辑非ECS模式下只有35帧，而ECS模式下帧率就到127帧。（每台电脑性能不一样，帧数有差别）
 * 
 */
namespace myPractice2.Example12.Demo2
{
    struct InitData :IComponentData
    {
        public float3 Value;
    }



    public class MyJobSystem : JobComponentSystem
    {


        //ECS模式就是将Mono Update中代码挪到Job中来修改坐标，
        //其中InitData是新加的组件用于保存Translation的初始坐标。
        [BurstCompile]
        private struct MyJob : IJobForEach<Translation, InitData>
        {
            public void Execute(ref Translation translation, [ReadOnly] ref InitData initData)
            {
                float3 pos = translation.Value;
                pos.y += 0.1f;
                if (pos.y > 5f)
                {
                    pos.y = initData.Value.y;
                }
                translation.Value = pos;
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new MyJob();
            return job.Schedule(this, inputDeps);
        }


    }

    public class Demo2:MonoBehaviour
    {
        public GameObject prefab;
        public int initCount;//根据电脑性能，修改个数，现在修改成10000

        EntityManager m_Manager;
        BlobAssetStore m_BlobAssetStore;
        Entity m_EntityPrefab;

        public bool useECS;

        List<GameObject> m_GameObjects;
        List<float3> m_InitPos;
        private void Start()
        {
            m_Manager = World.DefaultGameObjectInjectionWorld.EntityManager;
            m_BlobAssetStore = new BlobAssetStore();
            var m_setting = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, m_BlobAssetStore);
            m_EntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, m_setting);

            m_InitPos = new List<float3>();
            m_GameObjects = new List<GameObject>();
            for(int i = 0; i < initCount; i++)
            {
                Vector3 pos = UnityEngine.Random.insideUnitSphere * 5;

                if (!useECS)
                {
                    m_GameObjects.Add(Instantiate(prefab, pos,Quaternion.identity));
                    m_InitPos.Add(pos);
                }
                else
                {
                    var entity = m_Manager.Instantiate(m_EntityPrefab);
                    m_Manager.SetComponentData(entity, new Translation { Value = pos });
                    m_Manager.AddComponentData(entity, new InitData { Value = pos });
                }
            }
        }

        private void Update()
        {
            //非ECS模式就是在Update中对让所有游戏对象向上移动，当超过5米后返回初始坐标
            if (!useECS)
            {
                for(int i = 0; i < initCount; i++)
                {
                    var go = m_GameObjects[i];
                    float3 pos = go.transform.position;
                    pos.y += 0.1f;
                    if(pos.y > 5)
                    {
                        pos.y = m_InitPos[i].y;
                    }
                    go.transform.position = pos;
                }
            }
        }
        private void OnDestroy()
        {
            if (m_BlobAssetStore != null)
                m_BlobAssetStore.Dispose();
        }

    }
}
