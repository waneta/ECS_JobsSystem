﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

//12 对比ECS、非ECS的加载效率和更新效率
/* 12.1 对比ECS和非ECS的加载效率
 * 单纯的加载时间，ESC会提升3倍多
 * 
 */
namespace myPractice2.Example12.Demo1
{
    public class Demo1:MonoBehaviour
    {
        public GameObject prefab;
        public int initCount;

        EntityManager m_Manager;
        BlobAssetStore m_BlobAssetStore;
        Entity m_EntityPrefab;

        private void Start()
        {
            m_Manager = World.DefaultGameObjectInjectionWorld.EntityManager;
            m_BlobAssetStore = new BlobAssetStore();
            var m_setting = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, m_BlobAssetStore);
            m_EntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, m_setting);
        }

        private void OnGUI()
        {
            if (GUILayout.Button("非ECS"))
            {
                float t = Time.realtimeSinceStartup;
                for(int i = 0; i < initCount; i++)
                {
                    Vector3 pos = UnityEngine.Random.insideUnitSphere * 5;

                    Instantiate(prefab, pos, Quaternion.identity);
                }
                t = (Time.realtimeSinceStartup - t) * 1000f;
                Debug.LogFormat("Instantiate: {0} ms",t);
            }
            //
            if (GUILayout.Button("ECS"))
            {
                
                float t = Time.realtimeSinceStartup;
                for (int i = 0; i < initCount; i++)
                {
                    Vector3 pos = UnityEngine.Random.insideUnitSphere * 5;
                    Entity entity = m_Manager.Instantiate(m_EntityPrefab);
                    m_Manager.SetComponentData(entity, new Translation { Value = pos });

                }
                t = (Time.realtimeSinceStartup - t) * 1000f;
                Debug.LogFormat("ECS Instantiate: {0} ms", t);
            }
        }

        private void OnDestroy()
        {
            if (m_BlobAssetStore != null)
                m_BlobAssetStore.Dispose();
        }

    }
}
