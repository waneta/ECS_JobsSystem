﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

/*6.2 Entity对象的脚步
 * 
 * Prefab已经转成实体对象了，但是很多Prefab上是需要序列化数据的。比如，我们自己写个脚步挂在Prefab，再写些
 * public的属性来序列化。假设开始的脚本是这样，可以在面板上序列化int的值。
 * 
 * 
 * 
 */
namespace myPractice2.Example06.Demo2
{

    [System.Serializable]
    public struct TestEntity : IComponentData
    {
        public int value;
    }
    /*
     * 但是由于这个Prefab转成Entity了，所以这个脚本所序列的数据就没有了，其实我们可以监听游戏对象转Entity的事件，
     * 这样就能很方便地把数据集同时赋给实体对象，修改后的代码如下所示：
     */
    public class demo2 : MonoBehaviour, IConvertGameObjectToEntity
    {
        public int value;
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            //将数据添加到TestEntity实体
            dstManager.AddComponent(entity, typeof(TestEntity));
            TestEntity testEntity = new TestEntity { value = value };
            dstManager.AddComponentData(entity, testEntity);
        }
    }
    /*
     * 脚本转成实体对象以后原本的方法就不能执行了，实体组件只序列化数据并没有业务逻辑，所以如果要
     * 更新它们就要在系统里完成了。如下图所示，Entity debuger 实体序列化的数据已经添加进来了。
     * 
     * 
     * 
     */
}
