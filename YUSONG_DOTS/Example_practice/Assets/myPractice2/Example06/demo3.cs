﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

/*6.3 场景批量转Entity
 * Prefab我们已经转成实体了，其实我们还可以将整个场景批量转成Entity对象。
 * 在场景中选个root根节点，右键选择New Sub Scene From Selection就可以将这个节点生成场景文件
 * 
 * Root场景嵌套在父场景中，Sub Scene脚本上点击Close就可以将场景下的所有实体转成Entity对象了。
 * 
 * 这个方法确实方便，但是要记住并不是所有东西都能无缝转成Entity对象。
 * 比如，前面提到的粒子特效、动画。其实这些都是小事，我们可以让美术把只包含Mesh的对象放在一个
 * 一个Root节点下统一转成Entity,方案确实很完美，
 * 但是有个最重要的缺陷，那就是不支持Lightmap烘焙贴图，这显然无法接受。
 */
namespace myPractice2.Example06.Demo3
{


    public class demo3 : MonoBehaviour
    {
        
    }

}
