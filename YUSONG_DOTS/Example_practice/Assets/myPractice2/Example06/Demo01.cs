﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

//6 Entity对象的父子关系和脚本以及场景批量转Entity https://edu.uwa4d.com/lesson-detail/161/827/0?isPreview=0
/* 6.1 Entity对象的父子关系
 * 
 * Prefab上是可以记录父子关系的，Prefab转成Entity对象也具备这个关系，那么它是如何实现的呢？比如现在有一个Prefab的结构如下
 * Prefab:
 * ------Child1
 * ------Child2:
 * -------------Child2-1
 * 
 */
namespace myPractice2.Example06.Demo1
{
    

    public class Demo01 : MonoBehaviour
    {
        public GameObject prefab;
        Entity instance;
        void Start()
        {
            var manager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
            //将prefab转成Entity
            /*
             * 这里并非转成一个Entity对象，而是转成了4个Entity对象，Prefab、Child1、Child2、Child2-1。
             * 如图下图所示，Entity Debuger, Prefab的Entity对象通过LinkedEntityGroup组件连接了包括Prefab自身的一共四个字对象。
             * 
             */
            var entityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);


            /*
             * 实例化完后就是Clone了4个新的Entity,这样加上之前的4个Entity,目前一个8个实体对象了。
             * 如图下图所示，Entity Debuger中，右侧LinkedEntityGroup中的连接的就是Clone出来的4个实体，由于它们是有父子层级关系的
             * 往下看实体还会自动挂上Child组件用来关联这个实体对象的子对象，点击右边的Select按钮就可以选择子对象。
             * 如果子对象还有子对象，那么可以这样一直嵌套下去
             * 
             * 
             */
            instance = manager.Instantiate(entityPrefab);
            manager.SetName(instance, "Prefab(Clone)");


        }

        private void OnGUI()
        {
            if (GUILayout.Button("修改实体"))
            {
                var manager = World.DefaultGameObjectInjectionWorld.EntityManager;
                manager.SetComponentData(instance, new Translation { Value = Vector3.zero });
            }
            if (GUILayout.Button("删除实体"))
            {
                var manager = World.DefaultGameObjectInjectionWorld.EntityManager;
                manager.DestroyEntity(instance);
                /*
                 * 此时我们删掉的只是克隆出来的实体对象，而GameObjectConversionUtility.ConvertGameObjectHierarchy生成的4个实体对象并没有被删除掉。
                 * 刚开始以为它会在下次GC时被删掉，但是试了一下并不会。目前来看，最好自己手动manager.DestroyEntity释放掉它
                 * 
                 * 目前来看我们已经成功将Prefab无缝地转换成Entity对象了，但是这离商用还有很多问题，目前转成的Prefab只支持最简单的
                 * Mesh,并不支持粒子特效、骨骼动画烘焙贴图这些，如果转换的时候发现Prefab里有不支持的对象只会保留这个实体的Transform位置信息
                 * 
                 * 
                 * 
                 * 
                 */
            }
        }

    }
}
