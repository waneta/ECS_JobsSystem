﻿using UnityEngine;
using Unity.Entities;
using System.Collections.Generic;
using Unity.Scenes;
using Unity.Transforms;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Collections;

public class Example7 : MonoBehaviour
{
    public GameObject prefab; //加载的Prefab
    public GameObject hero; //英雄的游戏对象
    public int initCount; //加载的数量
    private EntityManager m_Manager;
    private Entity m_EntityPrefab;
    private BlobAssetStore m_BlobAssetStore;
    private float m_LastInitTime;

    private void Start()
    {
        //Prefab转成实体Prefab
        m_Manager = World.DefaultGameObjectInjectionWorld.EntityManager;
        m_BlobAssetStore = new BlobAssetStore();
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, m_BlobAssetStore);
        m_EntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
        m_LastInitTime = Time.time;
        World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<Example7RemoveSystem>().heroTransform = hero.transform;
        World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<Example7MoveSystem>().heroTransform = hero.transform;

    }

    public struct Example7Alias : IComponentData {
    }

    private void Update()
    {
        if (Time.time - m_LastInitTime >= 1f)
        {
            m_LastInitTime = Time.time;

            for (int i = 0; i < initCount; i++)
            {
                Vector3 pos = UnityEngine.Random.insideUnitSphere * 10f;
                Entity entity = m_Manager.Instantiate(m_EntityPrefab);
                m_Manager.SetComponentData(entity, new Translation { Value = pos });
                m_Manager.AddComponentData(entity, new Example7Alias());
            }
        }
    }

    public class Example7MoveSystem : JobComponentSystem
    {
        //外部负责传入中心灰色立方体的坐标
        public Transform heroTransform;
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            if (heroTransform != null)
            {
                float3 heroPos = heroTransform.position;
                float dt = Time.DeltaTime;
                float speed = 5f;
                var jobHandle = this.Entities.ForEach((ref Translation translation, ref Rotation rotation) =>
                    {
                    //每个符合条件的实体朝向中心灰色立方体移动
                    float3 heading = heroPos - translation.Value;
                        rotation.Value = quaternion.LookRotation(heading, math.up());
                        translation.Value = translation.Value + (dt * speed * math.forward(rotation.Value));
                    })
                    .Schedule(inputDeps);
                return jobHandle;
            }
            return inputDeps;
        }
    }
    //[AlwaysSynchronizeSystem]
    public class Example7RemoveSystem : JobComponentSystem
    {
        public Transform heroTransform;
        private EntityQuery m_Query;
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;
        protected override void OnCreate()
        {
            base.OnCreate();
            m_EndSimulationEcbSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            m_Query = GetEntityQuery(typeof(Example7Alias), typeof(Translation));
        }


        [BurstCompile]
        struct MyJob : IJobChunk
        {
            //传入坐标组件
            [ReadOnly] public ArchetypeChunkComponentType<Translation> translationType;
            //传入实体类型
            [ReadOnly] public ArchetypeChunkEntityType  entityType;
            public float3 heroPos;
            //缓冲区用于删除组件
            public EntityCommandBuffer.Concurrent CommandBuffer;
            //系统的版本号
            public uint LastSystemVersion;
            
            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
            {
                //通过系统版本号和组件类型比较是否发生变化
                var translationsChanged = chunk.DidChange(translationType, LastSystemVersion);
                //如果块中的Translation数组没有变化则不进行遍历
                if (!translationsChanged)
                    return;

                //取出坐标组件数组
                var translations = chunk.GetNativeArray(translationType);
                //取出坐标组件对应的实体数组
                var entities = chunk.GetNativeArray(entityType);


                for (var i = 0; i < translations.Length; i++)
                {
                    //当它们距离小于0.1米时删除
                    if (math.distance(translations[i].Value, heroPos) <= 0.1f)
                    {
                        //删除实体
                        CommandBuffer.DestroyEntity(chunkIndex,entities[i]);
                    }
                }
               
            }
        }
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            float3 heroPos = heroTransform.position;
            var myJob = new MyJob()
            {
                CommandBuffer = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent(),
                entityType = this.GetArchetypeChunkEntityType(),
                translationType = this.GetArchetypeChunkComponentType<Translation>(true),
                heroPos = heroPos,
                LastSystemVersion = this.LastSystemVersion

            };
            var jobHandle  = myJob.Schedule(m_Query, inputDeps);
            m_EndSimulationEcbSystem.AddJobHandleForProducer(jobHandle);
            return jobHandle;
        }

        /*下面代码等价于上面代码
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            float3 heroPos = heroTransform.position;
            var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent();
            var jobHandle = this.Entities
            .WithAll<Example7Alias>()
            .WithChangeFilter<Translation>() //监听变化的组件
            .ForEach((Entity entity, int entityInQueryIndex, Translation transflation) =>
            {
                //当它们距离小于0.1米时删除
                if (math.distance(transflation.Value, heroPos) <= 0.1f)
                {
                    ecb.DestroyEntity(entityInQueryIndex, entity);
                }
            })
            .WithoutBurst()
            .Schedule(inputDeps);
            m_EndSimulationEcbSystem.AddJobHandleForProducer(jobHandle);
            return jobHandle;
        }
        */
    }
   

    private void OnDestroy()
    {
        if (m_BlobAssetStore != null)
            m_BlobAssetStore.Dispose();

    }
}
