﻿
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using System;

//9.4 系统生命周期
/*
 * MySystem并没有在代码中被激活，但是它却自动执行了。其原因是Unity默认就激活了所有的System
 * 但是有时我们可能并不希望某些系统默认就是激活而是用到的时候在激活。
 * 可以在系统类头部加上[DisableAutoCreation]表示它不会被自动创建激活
 */
namespace myPractice.Example9.Demo4
{

    public struct PositionAndRotation : IComponentData
    {
        public float3 position;
        public quaternion rotation;

    }

    [DisableAutoCreation]
    public class MySystem : ComponentSystem
    {
        protected override void OnCreate()
        {
            //系统初始化调用，并且只会被调用一次
            base.OnCreate();
            Debug.LogError("===============MySystem OnCreate=================");
        }
        protected override void OnDestroy()
        {
            //系统被删除时调用
            base.OnDestroy();
            Debug.LogError("===============MySystem OnDestroy=================");
        }
        protected override void OnStartRunning()
        {
            //系统每次进入激活状态调用
            //每次 Enable = true;时调用
            base.OnStartRunning();
            Debug.LogError("===============MySystem OnStartRunning=================");
        }
        protected override void OnStopRunning()
        {
            //系统每次取消激活状态时调用
            //每次 Enable = false;时调用
            //系统被销毁时会优先调用，然后才会调用OnDestroy()
            base.OnStopRunning();
            Debug.LogError("===============MySystem OnStopRunning=================");
        }
        protected override void OnUpdate()
        {
            //正常更新
            Debug.Log("===============MySystem OnUpdate=================");
        }
    }
    /*
     * 总体来说，目前Sysytem官方是不建议运行动态创建或者销毁的，代码中也能体现动态创建或者销毁的麻烦
     * 游戏中可能会有多个System系统，那么它们的执行顺序就尤为重要，有时候可能需要调整多系统的依赖顺序。
     * UpdateBefore 、UpdateAfter
     */
    [UpdateBefore(typeof(MySystem))]
    public class MySystem2 : Component { }


 
    public class Demo4 : MonoBehaviour
    {
        public void Start()
        {


        }
        public void OnGUI()
        {
            if (GUILayout.Button("动态创建"))
            {
                Test1();
            }
            if (GUILayout.Button("启用，激活"))
            {
                //MySystem system = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MySystem>();
                system.Enabled = true;//好像不管用？？？
            }
            if (GUILayout.Button("取消激活"))
            {
                Test2();
            }
            if (GUILayout.Button("销毁"))
            {
                Test3();
            }
        }
        World world;
        MySystem system;
        SimulationSystemGroup simulationSystemGroup;
        //运行时找到合适的时间再动态创建它

        void Test1()
        {
            world = World.DefaultGameObjectInjectionWorld;
            system = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MySystem>();
            simulationSystemGroup = world.GetOrCreateSystem<SimulationSystemGroup>();
            ScriptBehaviourUpdateOrder.UpdatePlayerLoop(world);
        }
        //动态地启动/取消 它
        void Test2()
        {
            
            //MySystem system = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MySystem>();
            system.Enabled = false;
        }
        //彻底销毁某个系统
        void Test3()
        {
            //World world = World.DefaultGameObjectInjectionWorld;
            //MySystem system = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MySystem>();
            //var simulationSystemGroup = world.GetOrCreateSystem<SimulationSystemGroup>();
            
            simulationSystemGroup.RemoveSystemFromUpdateList(system);
            world.DestroySystem(system);

            ScriptBehaviourUpdateOrder.UpdatePlayerLoop(world);
        }

    }
}
