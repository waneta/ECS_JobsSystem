﻿
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using System;

//系统System

namespace myPractice.Example9.Demo1
{

    public struct PositionAndRotation : IComponentData
    {
        public float3 position;
        public quaternion rotation;

    }
    //组件系统不能包含实体的数据，所以它首先就需要找到它所关心的组件。
    //最简单的遍历方法就是在Update中将Entities.ForEach传入Lambad函数，指定关心的组件，也可以是多个关心的组件
    //ComponentSystem组件系统是在主线程中运行，所以无法利用多核并行处理
    public class MySystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref PositionAndRotation com) => {
                com.position = Time.DeltaTime + com.position;
                Debug.Log("com.position");


            });
        }
    }


 
    public class Demo1 : MonoBehaviour
    {
        public void Start()
        {
            

        }
    }
}
