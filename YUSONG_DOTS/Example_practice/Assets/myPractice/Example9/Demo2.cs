﻿using System;
using UnityEngine;
using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;


//JobComponentSystem任务组件系统
/* 
 * ESC里的Component已经具有超高的Cache命中率，性能已经比传统的脚本快不少，
 * 但是ComponentSystem还只是运行在主线程中，这就意味着每个ComponentSystem都必须等待上一个执行完毕才能执行自己的。
 * ESC要配合JobSystem才能让性能飞起来，所以我们可以使用JobComponetSystem任务组件系统
 * 
 * JSC表示JobComponentSyste,JSC在主线程创建Job后交给子线程去完成它。
 * 这样一来，每个JSC不需要上一个JSC执行完后在运行下一个，真正实现了多线程并行。
 * 
 * 如果都是只读的数据，确实可以并行完成，但是如果第一个JCS里 在改数据，此时第二个JCS在执行的时候
 * 必须要等待第一个JCS执行完毕，这就是我们注意的硬性同步点。
 * 
 * CS表示ComponentSystem, JSC可以和CS混合使用，它们都是按顺序执行的，只是JCS可以将一部分工作交到工作线程中
 * 如果JCS或者CS都是需要对实体创建、销毁、组件添加、组件删除等操作、修改共享组件的值，那么它必然会来带一次硬性同步点，这就会导致主线程卡顿
 * 
 * 如下代码，将Demo1 的CS方式 改成JCS方式
 */

namespace myPractice.Example9.Demo2
{

    public struct PositionAndRotation : IComponentData
    {
        public float3 position;
        public quaternion rotation;

    }

    [BurstCompile]
    public class MySystem : JobComponentSystem
    {
        [BurstCompile]
        struct MyJob:IJobForEach<PositionAndRotation>//找到系统关心的组件
        {
            public float time;

            public void Execute([WriteOnly] ref PositionAndRotation c0)
            {
                c0.position = time + c0.position;
            }
        }

        //看到OnUpdate(）方法需要返回JobHandle,它是结构体调用Schedule后其实就是开启一个任务
        //也可以理解队列，这个队列方便就在于我们只需要给里加，并不需要删除，当多线程梳理完毕Unity会自己删除
        //1.
        //protected override JobHandle OnUpdate(JobHandle inputDeps)
        //{
        //    //创建一个JOB
        //    MyJob job = new MyJob() { time = Time.time };//Time.time已经过时
        //    //交给多线程执行
        //    return job.Schedule(this, inputDeps);
        //}

        //2.
        //protected override JobHandle OnUpdate(JobHandle inputDeps)
        //{
        //    //创建一个JOB
        //    MyJob job = new MyJob() { time = Time.time };//Time.time已经过时
        //    //交给多线程执行
        //    JobHandle handle =  job.Schedule(this, inputDeps);
        //    //等待多线程，完成这个Job
        //    handle.Complete();
        //    return handle;
        //}

        //3.等价上面
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            //创建一个JOB
            MyJob job = new MyJob() { time = (float)Time.ElapsedTime };//Time.time已经过时

            return job.Run(this);//表示指定并且等待多线程结束
        }
        //4.
        /*如果现在有好几个Job并且想同时完成它们，可以先把它们加到一个队列，最后使用JobHandle.CompleteAll()
        protected  JobHandle OnUpdate2(JobHandle inputDeps)
        {

            

            NativeList<JobHandle> jobs = new NativeList<JobHandle>(Allocator.Temp);
            
            for(int i = 0; i < 1000; i++)
            {
                MyJob job = new MyJob() { time = Time.time };
                jobs.Add(job.Schedule(this,inputDeps));
            }
            JobHandle.CompleteAll(jobs);
            jobs.Dispose();

            return default;
        }*/

    }



    public class Demo2 : MonoBehaviour
    {
        public void Start()
        {
            

        }
    }
}
