﻿
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using System;

//9.3 Entity Command Buffers实体缓冲
/*
 * JCS的性能杀手就是硬性同步点，硬性同步点包括实体进行创建、销毁、组件添加、组件删除等操作、修改共享组件的值
 * 如果代码稍微写的不太严谨就会带来这些硬性同步点，使用Entity Command Buffers.
 * 
 * 1帧内代码内可能就会造成多次硬性同步点，Entity Command Buffers的原理
 * 先将这一帧内的硬性同步点先缓冲一下，等这一帧结束之前统一执行同步一次。
 * 这就好比本来这一帧需要硬性同步五次，用了Entiry Command Buffers就会减少到同步一次。
 * 
 * Unity提供了硬性同步点的类EndSimulationEntityCommandBufferSystem,我们也可以自己去继承
 * EntityCommandBufferSystem写一个新的类。
 * 
 */

namespace myPractice.Example9.Demo3
{

    public struct PositionAndRotation : IComponentData
    {
        public float3 position;
        public quaternion rotation;
        public bool delete;

    }
    //
    public class MySystem : JobComponentSystem
    {
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;
        protected override void OnCreate()
        {
            base.OnCreate();
            //获取ECS对象
            m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        //如下代码，将Job添加到ECS中，最后统一执行一次硬性同步点
        //遍历的时候删除实体是有问题的，所以我们可以先在缓冲器中标记需要删除的实体最后统一删除，这样就安全多了。
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent();
            var jobHandle = Entities.ForEach((Entity entity, int entityInQueryIndex, ref PositionAndRotation c0) =>
            {
                if (!c0.delete)
                {
                    c0.delete = true;
                    //删除实体
                    ecb.DestroyEntity(entityInQueryIndex, entity);
                }
            }).Schedule(inputDeps);

            //添加到ECB队列中，等待统一删除
            m_EndSimulationEcbSystem.AddJobHandleForProducer(jobHandle);
            return default;
        }
    }


 
    public class Demo3 : MonoBehaviour
    {
        public void Start()
        {
            

        }
    }
}
