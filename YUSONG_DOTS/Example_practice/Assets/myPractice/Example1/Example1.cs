﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Unity.Entities;

//DOTS的全程是Data-Oriented Tech Stack,多线程式数据导向型技术堆栈。
//原理就是充分利用CPU多核处理器，让游戏处理的速度更快，更高效。
//其实就是把原本MonoBehaviour中数据部分和逻辑部分分拆在IComponentData和ComponentSystem中

//1.1DOTS的组成部分
/*
 * 1.任务系统（Job System），可用于高效运行多线程代码
 * 2.实体组件系统（ECS),用于默认编写高性能代码
 * 3.Burst Compiler编译器，用于生成高度优化本地代码，提供单指令多数据（SIMD）。
 */
/*2.2高性能C#
 * HPC# 全称HIGHT PERFORMANCE C#
 * .NET Core比C++慢2倍
 * Mone 比 .NET Core慢3倍
 * IL2CPP 比 Mono 快2-3倍 ，比C++慢2倍
 * 使用Burst编译后可以让C#的代码运行效率比C++还要快
 **/
namespace myPractice.Example1
{
    public struct GameComponentData : IComponentData
    {
        public int x;
    }
    public class MyGameSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            this.Entities.ForEach((ref GameComponentData data) =>
            {
                data.x++;
            });
        }
    }
}
