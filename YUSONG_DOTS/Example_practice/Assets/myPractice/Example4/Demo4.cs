﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using UnityEngine.Profiling;

namespace myPractice.Example4.Demo4
{
    public class Demo4:MonoBehaviour
    {
        [BurstCompile]
        struct Job : IJobParallelFor
        {
            [ReadOnly] public NativeArray<int> array;
            [WriteOnly] public NativeQueue<int>.ParallelWriter queue;
            public void Execute(int index)
            {
                queue.Enqueue(array[index]);
            }
        }
        private void Start()
        {
            NativeArray<int> array = new NativeArray<int>(new int[] { 0, 1, 2, 3 }, Allocator.TempJob);
            NativeQueue<int> queue = new NativeQueue<int>(Allocator.TempJob);
            Job job = new Job
            {
                array = array,
                queue = queue.AsParallelWriter(),//转成多线程并行写入格式
            };

            job.Run(array.Length);
            array.Dispose();
            queue.Dispose();
        
        }
        

    }
}
