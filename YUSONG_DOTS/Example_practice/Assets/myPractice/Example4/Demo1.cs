﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using UnityEngine.Profiling;

namespace myPractice.Example4.Demo1
{
    public class Demo1:MonoBehaviour
    {
        [BurstCompile]
        struct Job : IJob
        {
            public NativeArray<float> values;
            public float offset;
            public void Execute()
            {
                for(int i = 0; i < values.Length; i++)
                {
                    values[i] = i / offset;
                }
            }
        }
        private void Update()
        {
            Profiler.BeginSample("Normal");
            float[] normal = new float[1000000];
            for (int i = 0; i < normal.Length; i++)
            {
                normal[i] = i / 5f;
            }

            Profiler.EndSample();

            Profiler.BeginSample("Job");
            NativeArray<float> values = new NativeArray<float>(1000000, Allocator.TempJob);
            var job = new Job
            {
                values = values,
                offset = 5f,
            };

            JobHandle jobHandle = job.Schedule();
            jobHandle.Complete();
            values.Dispose();
            Profiler.EndSample();
        }

    }
}
