﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using UnityEngine.Profiling;

namespace myPractice.Example4.Demo3
{
    public class Demo3:MonoBehaviour
    {
        [BurstCompile]
        struct Job : IJobParallelFor
        {
            [WriteOnly] public NativeArray<float> values;
            [ReadOnly] public float offset;
            public void Execute(int index)
            {
                values[index] = index / offset;
            }
        }
        private void Update()
        {


            for (int i = 0; i < 1000; i++) {
                
                NativeArray<float> values = new NativeArray<float>(500, Allocator.TempJob);
                var job = new Job
                {
                    values = values,
                    offset = 5f,
                };

                JobHandle jobHandle = job.Schedule(values.Length,32);
                jobHandle.Complete();
                values.Dispose();
                
            }
        
        }
        

    }
}
