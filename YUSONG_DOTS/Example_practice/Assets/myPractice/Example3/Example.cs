﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Unity.Collections;
using UnityEngine.Profiling;

namespace myPractice.Example3
{
    //HPC#应用
    //不产生GC,手动释放
    public class Example:MonoBehaviour
    {

        void UseNativeArray()
        {
            NativeArray<Vector3> nativeArray = new NativeArray<Vector3>(5, Allocator.Temp);
            
            for(int i = 0;i< nativeArray.Length; i++)
            {
                nativeArray[i] = Vector3.one * i;
            }
            nativeArray[2] = Vector3.one;
        }

        void UseNativeList()
        {
            //长度为5的原生数组列表
            NativeList<Vector3> nativeList = new NativeList<Vector3>(5, Allocator.Temp);
            nativeList.Add(Vector3.one);
            nativeList.Add(Vector3.one);
            //先将待删除的元素与数组最后一个元素进行交换，再删除最后一个元素
            nativeList.RemoveAtSwapBack(0);
        }

        void UseNativeQueue()
        {
            NativeQueue<Vector3> queue = new NativeQueue<Vector3>(Allocator.Temp);
            //队尾添加数据
            queue.Enqueue(Vector3.one);
            queue.Enqueue(Vector3.one);
            //队首取出数据
            Vector3 a = queue.Dequeue();
        }

        //NativeHashMap是哈希表，没有实现迭代器
        void UseNativeHashMap()
        {
            NativeHashMap<int, Vector3> hasMap = new NativeHashMap<int, Vector3>(5,Allocator.Temp);
            //添加数据
            hasMap[0] = Vector3.one;
            hasMap[1] = Vector3.one;
            //变量数据
            NativeArray<int> keys = hasMap.GetKeyArray(Allocator.Temp);

            for(int i = 0; i < keys.Length; i++)
            {
                int key = keys[i];
                Vector3 value = hasMap[key];
            }
        }

        //其他结构
        /*
         * NativeSlice
         * NativeMultiHashMap
         * NativeStream
         */

        //测试例子
        //Allocator.Persistent表示一个全局保存的数据结构,分配速度最慢
        //Allocator.Temp 表示临时对象，分配速度最快，在方法体中使用比较好
        //Allocator 还有Invalid、None、TempJob(分配速度仅次于Temp）
        void Update()
        {
            Profiler.BeginSample("NativeArray");
            NativeArray<Vector3> velocity = new NativeArray<Vector3>(1000, Allocator.Persistent);
            for(int i = 0; i < velocity.Length; i++)
            {
                velocity[i] = Vector3.zero;
            }

            velocity.Dispose();
            Profiler.EndSample();
            

            Profiler.BeginSample("Vector3[]");
            Vector3[] velocity1 = new Vector3[1000];
            for(int i= 0; i < velocity1.Length; i++)
            {
                velocity1[i] = Vector3.zero;
            }
            Profiler.EndSample();
        }
    }
}
