﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

using Unity.Entities;
using Unity.Jobs;
// 组与覆盖
/*  * 
 */

namespace myPractice.Example12.demo2
{

    //这个组件在第三方库中，我们无法修改它。System会遍历所有包含组件A的实体组件
    //来给角色做变颜色需求，甚至有可能创建实体的过程也是在第三方类库中实现的，我们并无法干预
    public struct A : IComponentData
    {
        public Color color;
    }

    public class ASystem : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var jobHandle = Entities.ForEach((ref A a) => {

                //处理变色...
                a.color = Color.red;
                Debug.Log("Color.red");

            }).WithEntityQueryOptions(EntityQueryOptions.FilterWriteGroup)
            .WithBurst()
            .Schedule(inputDeps);

            return jobHandle;
        }
    }

    /* 
     * 1.WriteGroup 相当于把使用了目标类型的Component放到了query里面的None组里面Component
     * 2.需要对方System Query开启EntityQueryOptions.FilterWriteGroup才行
     *  EntityQueryDesc介绍不够详细
     * 在这里意思，如果A和B组件同时添加在一个实体上，A组件的系统不再更新
     */

    [WriteGroup(typeof(A))]
    public struct B : IComponentData
    {
        public Color color;
    }

    /*
     * 如果场景中有两个实体，1号实体绑定了A组件，2号实体同时绑定了A、B组件
     * 1号实体在第三方库（我们无法修改）的A系统正常更新A组件
     * 2号实体由于B组件覆盖了A组件，所以在第三方库中（我们无法修改）的A系统将不再更新2号实体绑定的A组件，只会更新组件B
     */
    public class BSystem : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var jobHandle = Entities.ForEach((ref A a) => {
               
                //处理变色...
                Debug.Log("Color.black");
                a.color = Color.red;
            })
            .WithBurst()
            .Schedule(inputDeps);

            return jobHandle;
        }
    }



    class demo1:MonoBehaviour
    {
        private void Start()
        {
            //获取默认世界，实体管理器
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            //创建一个实体，并且绑定组件
            var entity = entityManager.CreateEntity(typeof(A));

            /*
             * 比如现在我们要新写一个组件B,其实B的目的其实和A一样也是改颜色
             * 但是由于A在第三方类库里我们无法修改它
             * 如下所示，我们自己给实体再加一个B组件
             */
            entityManager.AddComponentData<B>(entity, new B());
            //此时该实体，就同时包含A组件和B组件，这样意味着关心A系统和关心B系统都会同时执行上色的逻辑，
            //这显然不是我们想要的。我们希望，如果实体被添加了A和B组件以后，A系统不再执行只执行B系统，从而实现覆盖的可能。
        }
    }
}
