﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

using Unity.Entities;
using Unity.Jobs;
// 组与覆盖
/* Sytem系统写到后面可能会很多，而且有可能我们也会在packageManager下载一些第三方的包
 * 比如我们现在有变色的功能，代码如下
 * 
 * 
 */

namespace myPractice.Example12.demo1
{

    //这个组件在第三方库中，我们无法修改它。System会遍历所有包含组件A的实体组件
    //来给角色做变颜色需求，甚至有可能创建实体的过程也是在第三方类库中实现的，我们并无法干预
    public struct A : IComponentData
    {
        public Color color;
    }

    public class ASystem : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var jobHandle = Entities.ForEach((ref A a) => {
                a.color = Color.red;
                //处理变色
                Debug.Log("Color.red");

            }).WithEntityQueryOptions(EntityQueryOptions.FilterWriteGroup)
            .WithBurst()
            .Schedule(inputDeps);

            return jobHandle;
        }
    }
    class demo1:MonoBehaviour
    {
        private void Start()
        {
            //获取默认世界，实体管理器
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            //创建一个实体，并且绑定组件
            var entity = entityManager.CreateEntity(typeof(A));
        }
    }
}
