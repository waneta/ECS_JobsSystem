﻿
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using System;

//10 lambda访问实体数据
/*
 * 
 */

namespace myPractice.Example10.Demo3_1
{

    public struct A : IComponentData
    {
        public float total;

    }
    public struct B : IComponentData
    {
        public float total;
    }
    //
    public class MySystem : JobComponentSystem
    {
        
        protected override void OnCreate()
        {
            base.OnCreate();
           
        }
        //如果是共享组件的查询需要使用WithSharedComponentFilter
        //如果想在Entities.ForEach的lambda函数之前使用查询可以使用WithStoreEntityQueryInField.
        //因为我们并没有IJbo类，那么Burst编译选项该如何声明呢？如下代码
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var jobHandle = Entities.ForEach((ref A a, in B b) =>
            {
                a.total += b.total;

            }).WithBurst(FloatMode.Default)//使用Burst编译
             .Schedule(inputDeps);
            return jobHandle;
        }
        //默认情况下Unity就已经开启了lambda的burst的编译选项，由于Burst开启后并不能使用堆对象，所以也可以使用WithoutBurst禁用Burst编译

    }



    public class Demo3_1 : MonoBehaviour
    {
        public void Start()
        {


        }
    }
}
