﻿
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using System;

//10 lambda访问实体数据
/*
 * Entities.ForEach的方法是先遍历感兴趣的组件，然后在执行Schedule多线程执行逻辑代码，但是有时候可能就是单独实现一部分代码的初始化
 * 不需要依赖遍历组件，并且想写在多线程中Job中完成赋值
 */

namespace myPractice.Example10.Demo2
{


    public class MySystem : JobComponentSystem
    {
        
        protected override void OnCreate()
        {
            base.OnCreate();
           
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            NativeArray<float> numbers = new NativeArray<float>(500, Allocator.TempJob);
            for(int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = i;
            }
            //..继续执行下面的JOB任务
            return inputDeps;
        }
    }



    public class Demo2 : MonoBehaviour
    {
        public void Start()
        {


        }
    }
}
