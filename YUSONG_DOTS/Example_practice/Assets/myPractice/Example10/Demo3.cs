﻿
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using System;
using Unity.Transforms;

//10 lambda访问实体数据
/* 
 * 
 */

namespace myPractice.Example10.Demo3
{
    public struct Destination : IComponentData
    {
        public float total;

    }
    public struct Source : IComponentData
    {
        public float total;
    }
    public class MySystem : JobComponentSystem
    {
        
        protected override void OnCreate()
        {
            base.OnCreate();
           
        }
        //首先找到LocalToWorld组件，然后必须包含任意Rotation、Translation、Scale组件
        //并且不能包含LocalToParent组件
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            return Entities.WithAll<LocalToWorld>()
                 .WithAny<Rotation, Translation, Scale>()
                 .WithNone<LocalToParent>()
                 .ForEach((ref Destination outputData, in Source inputData) =>
                 {
                     /* do some work */
                 }).Schedule(inputDeps);

        }
    }



    public class Demo3 : MonoBehaviour
    {
        public void Start()
        {


        }
    }
}
