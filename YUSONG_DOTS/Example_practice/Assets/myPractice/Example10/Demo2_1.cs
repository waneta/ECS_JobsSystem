﻿
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using System;

//10 lambda访问实体数据
/*
 * 改造上个例子，可以使用Job.WithCode来单独执行一段代码的多线程
 * 这样就可以解决不需要遍历组件也可以多线程处理代码逻辑了
 */

namespace myPractice.Example10.Demo2_1
{

    public class MySystem : JobComponentSystem
    {
        
        protected override void OnCreate()
        {
            base.OnCreate();
           
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            NativeArray<float> numbers = new NativeArray<float>(500, Allocator.TempJob);
            JobHandle generateNumbers = Job.WithCode(() =>
            {
                //多线程中赋值
                for (int i = 0; i < numbers.Length; i++)
                {
                    numbers[i] = i;
                }
            }).Schedule(inputDeps);
            //等待多线程完成数据的准备，后续子任务就可以使用numbers来工作了
            generateNumbers.Complete();

            //..继续执行下面的JOB任务
            return inputDeps;
        }
    }



    public class Demo2_1 : MonoBehaviour
    {
        public void Start()
        {


        }
    }
}
