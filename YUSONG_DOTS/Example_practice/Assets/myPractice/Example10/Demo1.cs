﻿
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using System;

//10 lambda访问实体数据
/*
 * 
 * 
 * 
 */

namespace myPractice.Example10.Demo1
{

    public struct A : IComponentData
    {
        public float total;

    }
    public struct B : IComponentData
    {
        public float total;
    }
    //
    public class MySystem : JobComponentSystem
    {
        
        protected override void OnCreate()
        {
            base.OnCreate();
           
        }
        
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var jobHandle = Entities.ForEach((ref A a, in B b) =>
            {
                a.total += b.total;

            }).Schedule(inputDeps);
            return jobHandle;
        }
    }



    public class Demo3 : MonoBehaviour
    {
        public void Start()
        {


        }
    }
}
