﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;

//Burst编译器高级用法
//一旦标记了[BurstCompile]就意味着Burst编译已经开启，它限制了代码
//中不能包含堆内存对象。如下代码所示，因为关闭了[BurstCompile]
//所以Job结构体方法中可以new class类对象，只是这样会产生GC

namespace myPractice.Example5.Demo1
{
    class A
    {
        public int a;
    }
    //[BurstCompile]
    struct Job : IJobParallelFor
    {
        [WriteOnly] public NativeArray<float> values;
        [ReadOnly] public float offset;
        public void Execute(int index)
        {
            values[index] = index / offset;
            A a = new A() { a = 200 };
            Debug.Log(a.a);
        }

        
    }
    public class Demo1 : MonoBehaviour
    {
        
    }
}
