﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;

//Burst编译器高级用法
/*
 * 如果标记了[BurstCompile]，那么上述代码就会报错，因为[BurstCompile]
 * 严格限制代码中不能出现堆内存数据。其实我们可以将使用堆内存代码放入另一个方法中
 * 方法前声明[BurstDiscard]表示不进行[BurstCompile]编译。
 * 如下代码所示，这样就不会报错了。
 * 所以如果Job类中的代码较多，并且有部分需要使用堆内存对象，我们可以采用这种
 * 方式将他们分开，一部分代码进行Burst编译，另一部分不进行Burst编译
 */

namespace myPractice.Example5.Demo2
{

    public class Demo2 : MonoBehaviour
    {
        class A
        {
            public int a;
        }
        [BurstCompile]
        struct Job : IJobParallelFor
        {
            [WriteOnly] public NativeArray<float> values;
            [ReadOnly] public float offset;
            public void Execute(int index)
            {
                values[index] = index / offset;
                MethodToDiscard();
            }


        }

        [BurstDiscard]
        static void MethodToDiscard()
        {
            A a = new A() { a = 200 };
            Debug.Log(a.a);
        }

    }
}
