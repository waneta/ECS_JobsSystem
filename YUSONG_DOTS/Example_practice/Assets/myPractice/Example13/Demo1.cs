﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Unity.Entities;
//13.1 版本与数据的改变
/* entity.Version//实体版本号
 * System.LastSystemVersion //系统版本号
 * Chunk.ChangeVersion[] //块中每个数组的版本
 * EntityManager.m_ComponentTypeOrderVersion[]//实体管理器中组件版本号
 * SharedComponentDataManager.m_SharedComponentVersion//共享组件管理器组件版本号
 * 
 * 
 */

namespace myPractice.Example13.demo1
{
    public struct A : IComponentData
    {
        public float total;

    }


    class Demo1:MonoBehaviour
    {
        private void Start()
        {
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var entity = entityManager.CreateEntity(typeof(A));

            Debug.LogError("entity.Version:"+entity.Version);



        }

    }
}
