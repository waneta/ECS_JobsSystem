﻿
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using System;

//11.1 IJob访问实体数据
/*
 * 
 */

namespace myPractice.Example11.Demo1_1
{
    struct RotationQuaternion : IComponentData
    {

    }

    struct RotationSpeed : IComponentData
    {

    }

    struct Frozen : IComponentData
    {

    }

    struct Gravity : IComponentData
    {

    }



    [ExcludeComponent(typeof(Frozen))]//不能包含某个组件
    [RequireComponentTag(typeof(Gravity))]//必须包含某个组件
    [BurstCompile]
    struct RotationSpeedJob : IJobForEach<RotationQuaternion, RotationSpeed>
    {
        public void Execute(ref RotationQuaternion c0, ref RotationSpeed c1)
        {
            throw new NotImplementedException();
        }
    }
    public class MySystem : JobComponentSystem
    {
        
        protected override void OnCreate()
        {
            base.OnCreate();
           
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            //throw new NotImplementedException();
            return default;
        }
    }



    public class Demo1_1 : MonoBehaviour
    {
        public void Start()
        {


        }
    }
}
