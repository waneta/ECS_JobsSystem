﻿
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using System;
using Unity.Collections;

//11.1 IJob访问实体数据
/*
 * 
 */

namespace myPractice.Example11.Demo1
{

    public struct A : IComponentData
    {
        public float total;

    }
    public struct B : IComponentData
    {
        public float total;
    }
    
    [BurstCompile]
    struct MyJob : IJobForEach<A, B>
    {
        public void Execute(ref A a,ref B b)
        {
            a.total += b.total;
        }

        //public void Execute([WriteOnly] ref A a,[ReadOnly] ref B b)
        //{
        //    a.total += b.total;
        //}
    }
    public class MySystem : JobComponentSystem
    {
        
        protected override void OnCreate()
        {
            base.OnCreate();
           
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            //var jobHandle = Entities.ForEach((ref A a, in B b) =>
            //{
            //    a.total += b.total;

            //}).Schedule(inputDeps);
            return default;
        }
    }



    public class Demo1 : MonoBehaviour
    {
        public void Start()
        {


        }
    }
}
