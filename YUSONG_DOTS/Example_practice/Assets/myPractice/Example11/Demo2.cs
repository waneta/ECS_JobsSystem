﻿
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using System;
using Unity.Collections;

//11.2 IJobChunk 访问实体数据
/*
 * 每个Archetype有16个字节，也就是一个Chunk块。使用IJobChun来访问实体数据是最灵活的
 * 因为每个数据都可以自己灵活控制，而这些是IJobForEach不具备的。
 * 如下代码，在OnCreate方法中我们来设置实体查询器，EntityQueryDesc类则用于描述查找组件的范围。
 */

namespace myPractice.Example11.Demo2
{
    public struct A : IComponentData
    {
        public float total;
    }

    public struct B : IComponentData
    {
        public float total;
    }

    public struct C : IComponentData
    {
        public float total;

    }

    /*
     * 如下代码，因为是块遍历，所以Execute方法中可以拿到块对象Chunk,块索引ChunkIndex以及块中第一个实体对象索引firstEntityIndex.
     * 处理数据是只需要根据块对象拿到Archetype原型数据对象，就可以对它们进行遍历了。
     */
    [BurstCompile]
    struct UpdateJob : IJobChunk
    {
        public ArchetypeChunkComponentType<B> b;
        [ReadOnly] public ArchetypeChunkComponentType<C> c;
        public uint LastSystemVersion;
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var inputAchanged = chunk.DidChange(b, LastSystemVersion);
            //判断数据是否发生变化，如果变化才能计算
            if (!(inputAchanged))
            {
                return;
            }
            //通过块对象得到这个块中Archetype的数组
            var inputAs = chunk.GetNativeArray(b);
            var readOnly = chunk.GetNativeArray(c);
            //有了数据就可以对它们进行计算了
            for(var i = 0;i< inputAs.Length; i++)
            {
                inputAs[i] = new B { total = inputAs[i].total + readOnly[i].total };
            }
        }
    }

    public class MySystem : JobComponentSystem
    {
        private EntityQuery m_Query;
        
        //注意：其中A、B、C都是通用实体组件，ComponentType.ReadWirte<B>()
        //和ComponentType.ReadOnly<C>()用于描述组件读写状态
        protected override void OnCreate()
        {
            base.OnCreate();
            var queryDescription = new EntityQueryDesc()
            {
                //不能包含的组件
                None = new ComponentType[]
                {
                    typeof(A)
                },
                //必须任意包含的组件，B 和 C 组件任意有一个绑定在实体上即可
                Any = new ComponentType[]
                {
                    typeof(B),
                    typeof(C)
                },
                //必须包含的组件，B 和C必须同时绑定在实体上
                All = new ComponentType[]
                {
                    ComponentType.ReadWrite<B>(),
                    ComponentType.ReadOnly<C>()
                }
            };

            //得到实体查询器
            m_Query = GetEntityQuery(queryDescription);
           
        }

        //在OnUpdate中开启一个Job任务。因为我们使用块来进行遍历
        //需要把块的类型传入Job中，并且还需要指定读写状态
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {

            var job = new UpdateJob()
            {
                //获取当前系统的版本
                LastSystemVersion = this.LastSystemVersion,
                //false 表示读写，true 表示只读
                b = GetArchetypeChunkComponentType<B>(false),
                c = GetArchetypeChunkComponentType<C>(true)
            };
            //开始异步执行
           return job.Schedule(m_Query,inputDeps);
        }


    }
        
      
    



    public class Demo2 : MonoBehaviour
    {
        public void Start()
        {
            //创建实体并且给组件初始化赋值
            //获取默认世界，实体管理器
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            //创建一个实体，并且绑定组件
            var entity = entityManager.CreateEntity(typeof(A), typeof(B), typeof(C));
            //给实体中某个组件赋值
            entityManager.SetComponentData<C>(entity, new C() { total = 100 });

        }
    }
}
