﻿
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using System;

//组件Component

namespace myPractice.Example8.Demo1
{
    //1.通用组件
    public struct PositionAndRotation : IComponentData
    {
        public float3 position;
        public quaternion rotation;

    }
    //2.共享组件
    //注意代码中必须实现IEquatable<T>接口，其实就是为了正确地比较两个
    //共享组件是否相等，这我们将Mesh和Material的HashCode异或在一起，得到
    //一个唯一的哈希
    public struct RendererMesh : ISharedComponentData,IEquatable<RendererMesh>
    {
        public Mesh mesh;
        public Material material;
        public bool Equals(RendererMesh other)
        {
            return mesh == other.mesh && material == other.material;
        }
        public override int GetHashCode()
        {
            int hash = 0;
            if (!ReferenceEquals(mesh, null)) hash ^= mesh.GetHashCode();
            if (!ReferenceEquals(material, null)) hash ^= material.GetHashCode();
            return hash;
        }

    }

 
    public class Demo1 : MonoBehaviour
    {
        public void Start()
        {
            //获取默认世界，实体管理器
            EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            //创建一个实体，并且绑定通用组件和共享组件
            entityManager.CreateEntity(typeof(PositionAndRotation), typeof(RendererMesh));

            //PositionAndRotation组件会保存在Archetype中,共享组件RendererMesh单独放在共享组件管理器中。
            
            //其他组件
            //1.SystemStateComponentData(系统状态组件）
            /*
             * ECS并没有提供事件和回调，比如1个组件被添加、删除，组件中的数字被修改是别的系统并不知道。
             * 系统状态组件就是用来解决这个问题的，比如现在有一个C组件，分配方法前面介绍了。还有一个SC组件
             * 它比并不需要我们来分配，而是系统自动完成分配。
             * 例如：当C组件对应的System对C进行修改，会同时同步给SC组件
             * C （IComponentData,用户分配）
             * SC（ISystemComponentData,系统分配）
             */
            //2.IBufferElementData(动态缓冲区）
            /*
             * 通用组件IComponentData必须搭配Burst才能让性能发挥到极致，这也就意味着无法在Job中使用堆内存数据
             * 所以IComponentData结构体中就不能创建数组。虽然也可以用共享组件ISharedComponentData,但是这样性能就会差很多
             * 此时我们就可以使用动态缓冲器来完成。
             */

            //3.块组件
            /*Archetype 一块一块组成，每16字节表示一块。但是之前的方式，块的分配全由系统完成，如下代码所示，我们可以给实体绑定块组件
             * 如果是块组件，在读写的时候就可以按块的方式来遍历了，这样会更加灵活
             */
            //创建一个实体，并且绑定共享组件
            var entity = entityManager.CreateEntity(typeof(RendererMesh));
            //给实体绑定块组件
            entityManager.AddChunkComponentData<PositionAndRotation>(entity);

        }
    }
}
