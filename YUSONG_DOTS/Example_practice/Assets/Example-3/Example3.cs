﻿using UnityEngine;
using Unity.Entities;
using System.Collections.Generic;
using Unity.Scenes;

public class Example3 : MonoBehaviour
{
    const int CHUCK_COUT = 4;
    public Transform hero;
    public SubScene[] subScenes;
    private SceneSystem m_SceneSystem;
    private int m_LastX = -1, m_LastZ=-1;
    private Dictionary<int, SubScene> m_LastLoadScene = new Dictionary<int, SubScene>();
    private HashSet<int> m_CurrentLoadSceneIndex = new HashSet<int>();


    private void Start()
    {
        m_SceneSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<SceneSystem>();
    }

    private void Update()
    {
        int x = (int)(hero.transform.position.x / 10f);
        int z = (int)(hero.transform.position.z / 10f);
        bool isChange = (x != m_LastX) || (z != m_LastZ);
        if (isChange)
        {
            m_LastX = x;
            m_LastZ = z;
            m_CurrentLoadSceneIndex.Clear();
            for (int i = x - 1; i <= x + 1; i++)
            {
                for (int j = z - 1; j <= z + 1; j++)
                {
                    if (i >= 0 && j >= 0 && i < CHUCK_COUT && j < CHUCK_COUT)
                    {
                        int index = j * CHUCK_COUT + i;
                        if (index < subScenes.Length)
                        {
                            m_LastLoadScene[index] = subScenes[index];
                            m_CurrentLoadSceneIndex.Add(index);
                            m_SceneSystem.LoadSceneAsync(subScenes[index].SceneGUID);
                        }
                    }
                }
            }
            //卸载不再当前九宫格中的块
            List<int> keys = new List<int>(m_LastLoadScene.Keys);
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                if (!m_CurrentLoadSceneIndex.Contains(key))
                {
                    m_SceneSystem.UnloadScene(m_LastLoadScene[key].SceneGUID);
                    m_LastLoadScene.Remove(key);
                }
            }
        }
    }
}
