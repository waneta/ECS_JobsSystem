﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
/// <summary>
/// 比较加载时间
/// </summary>
public class Example1 : MonoBehaviour
{
    public GameObject prefab; //加载的Prefab
    public int initCount; //加载的数量

    private EntityManager m_Manager;
    private Entity m_EntityPrefab;
    private BlobAssetStore m_BlobAssetStore;


    private void Start()
    {
        //Prefab转成实体Prefab
        m_Manager = World.DefaultGameObjectInjectionWorld.EntityManager;
        m_BlobAssetStore = new BlobAssetStore();
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, m_BlobAssetStore);
        m_EntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);


        float t = Time.realtimeSinceStartup;
        for (int i = 0; i < initCount; i++)
        {
            Vector3 pos = UnityEngine.Random.insideUnitSphere * 5f;
            Instantiate(prefab, pos, Quaternion.identity);
        }
        t = (Time.realtimeSinceStartup - t)  * 1000f;
        Debug.LogFormat("Instantiate: {0} ms" , t);

        float t1 = Time.realtimeSinceStartup;
        for (int i = 0; i < initCount; i++)
        {
            Vector3 pos = UnityEngine.Random.insideUnitSphere * 5f;
            Entity entity = m_Manager.Instantiate(m_EntityPrefab);
            m_Manager.SetComponentData(entity, new Translation { Value = pos });
        }
        t1 = (Time.realtimeSinceStartup - t1) * 1000f;
        Debug.LogFormat("ECS Instantiate: {0} ms" , t1);
    }

    private void OnDestroy()
    {
        if (m_BlobAssetStore != null) 
            m_BlobAssetStore.Dispose();
    }
}
