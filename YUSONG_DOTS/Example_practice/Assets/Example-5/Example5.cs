﻿using UnityEngine;
using Unity.Entities;
using System.Collections.Generic;
using Unity.Scenes;
using Unity.Transforms;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Collections;

public class Example5 : MonoBehaviour
{
    public GameObject prefab; //加载的Prefab
    public GameObject hero; //英雄的游戏对象
    public int initCount; //加载的数量
    private EntityManager m_Manager;
    private Entity m_EntityPrefab;
    private BlobAssetStore m_BlobAssetStore;
    private float m_LastInitTime;
    private void Start()
    {
        //Prefab转成实体Prefab
        m_Manager = World.DefaultGameObjectInjectionWorld.EntityManager;
        m_BlobAssetStore = new BlobAssetStore();
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, m_BlobAssetStore);
        m_EntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
        m_LastInitTime = Time.time;
        World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<Example5MoveSystem>().heroTransform = hero.transform;
    }

    public struct Example5Alias : IComponentData{}

    private void Update()
    {
        if(Time.time - m_LastInitTime >= 1f)
        {
            m_LastInitTime = Time.time;

            for (int i = 0; i < initCount; i++)
            {
                Vector3 pos = UnityEngine.Random.insideUnitSphere * 10f;
                Entity entity = m_Manager.Instantiate(m_EntityPrefab);
                m_Manager.SetComponentData(entity, new Translation { Value = pos });
                m_Manager.AddComponentData(entity, new Example5Alias());
            }
        }
    }

    public class Example5MoveSystem : JobComponentSystem
    {
        //外部负责传入中心灰色立方体的坐标
        public Transform heroTransform;
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;
        protected override void OnCreate()
        {
            base.OnCreate();
            m_EndSimulationEcbSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            float3 heroPos = heroTransform.position;
            float dt = Time.DeltaTime;
            float speed = 5f;
            var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent();

            var jobHandle = this.Entities.WithAll<Example5Alias>()
                .ForEach((Entity entity, int entityInQueryIndex, ref Translation translation, ref Rotation rotation) =>
                {
                    //每个符合条件的实体朝向中心灰色立方体移动
                    float3 heading = heroPos - translation.Value;
                    rotation.Value = quaternion.LookRotation(heading, math.up());
                    translation.Value = translation.Value + (dt * speed * math.forward(rotation.Value));
                    //当它们距离小于0.1米时加入缓冲区
                    if (math.distance(translation.Value, heroPos) <= 0.1f)
                    {
                        ecb.DestroyEntity(entityInQueryIndex, entity);
                        //还可以使用缓冲器来创建实体，或者添加删除组件
                        //var e =  ecb.CreateEntity(entityInQueryIndex);
                        //ecb.AddComponent<Translation>(entityInQueryIndex, e);
                    }
                })
                .Schedule(inputDeps);
            //统一执行缓冲区的操作
            m_EndSimulationEcbSystem.AddJobHandleForProducer(jobHandle);
            return jobHandle;
        }
    }
    /*  下面代码和上面代码功能一样，只是放在主线程中执行
    public class Example5MoveSystem : ComponentSystem
    {
        public Transform heroTransform;
        protected override void OnUpdate()
        {
            float3 heroPos = heroTransform.position;
            float dt = Time.DeltaTime;
            float speed = 5f;
            this.Entities.WithAll<Example5Alias>()
                .ForEach((Entity entity, ref Translation translation, ref Rotation rotation) =>
                {
                    //每个符合条件的实体朝向中心灰色立方体移动
                    float3 heading = heroPos - translation.Value;
                    rotation.Value = quaternion.LookRotation(heading, math.up());
                    translation.Value = translation.Value + (dt * speed * math.forward(rotation.Value));
                    //当它们距离小于0.1米时加入缓冲区
                    if (math.distance(translation.Value, heroPos) <= 0.1f)
                    {
                        //添加到缓冲器，等待最后执行
                        PostUpdateCommands.DestroyEntity(entity);
                    }
                });
        }
    }
    */

    private void OnDestroy()
    {
        if (m_BlobAssetStore != null)
            m_BlobAssetStore.Dispose();

    }
}
