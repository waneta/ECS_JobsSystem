﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;
/// <summary>
/// 比较加载时间
/// </summary>
public class Example2 : MonoBehaviour
{
    public GameObject prefab; //加载的Prefab
    public int initCount; //加载的数量
    public bool useEcs; //是否使用ECS
    private EntityManager m_Manager;
    private Entity m_EntityPrefab;
    private BlobAssetStore m_BlobAssetStore;

    private List<GameObject> m_GameObjects;
    private List<float3> m_InitPos;
    private void Start()
    {
        //Prefab转成实体Prefab
        m_Manager = World.DefaultGameObjectInjectionWorld.EntityManager;
        m_BlobAssetStore = new BlobAssetStore();
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, m_BlobAssetStore);
        m_EntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
        m_InitPos = new List<float3>();

        
        m_GameObjects = new List<GameObject>();
        for (int i = 0; i < initCount; i++)
        {
            Vector3 pos = UnityEngine.Random.insideUnitSphere * 5f;
         
            if (useEcs)
            {
                Entity entity = m_Manager.Instantiate(m_EntityPrefab);
                m_Manager.SetComponentData(entity, new Translation { Value = pos });
                m_Manager.AddComponentData(entity, new InitData { Value = pos });
            }
            else
            {
                m_GameObjects.Add(Instantiate(prefab, pos, Quaternion.identity));
                m_InitPos.Add(pos);
            }
        }
    }


    private void Update()
    {
        if (!useEcs)
        {
            for (int i = 0; i < initCount; i++)
            {
                var go = m_GameObjects[i];
                float3 pos = go.transform.position;
                pos.y += 0.1f;
                if (pos.y > 5f)
                {
                    pos.y = m_InitPos[i].y;
                }
                go.transform.position = pos;
            }
        }
    }

    public struct InitData : IComponentData
    {
        public float3 Value;
    }

    public class MyJobSystem : JobComponentSystem
    {
        [BurstCompile]
        private struct MyJob : IJobForEach<Translation, InitData>
        {
            public void Execute(ref Translation translation, [ReadOnly] ref InitData initData)
            {
                float3 pos = translation.Value;
                pos.y += 0.1f;
                if (pos.y > 5f)
                {
                    pos.y = initData.Value.y;
                }
                translation.Value = pos;
            }
        }
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new MyJob();
            return job.Schedule(this, inputDeps);
        }
            
    }


    private void OnDestroy()
    {
        if (m_BlobAssetStore != null) 
            m_BlobAssetStore.Dispose();

    }
}
