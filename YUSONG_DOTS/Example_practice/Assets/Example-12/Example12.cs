﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Collections;

public class Example12 : MonoBehaviour
{
    
        
    public GameObject prefab;
    public int initCount;
    public bool useEcs;

    List<Transform> m_Transforms = new List<Transform>();
    NativeArray<quaternion> m_Rotations;
    private void Start()
    {
        m_Rotations =  new NativeArray<quaternion>(initCount, Allocator.Persistent);
        for (int i = 0; i < initCount; i++)
        {
            var go = Instantiate(prefab);
            go.name = "Dude_low";
            go.transform.position = UnityEngine.Random.insideUnitCircle * 10f;

            m_Transforms.Add(go.transform);
        }

    }

   
    private void Update()
    {
        float startTime = Time.realtimeSinceStartup;
        if (useEcs)
        {
            MyJob job = new MyJob();
            job.frameCount = Time.frameCount;
            job.rotations = m_Rotations;
            job.Schedule(initCount, 54).Complete();// 放到job中完成
           
            for (int i = 0; i < m_Transforms.Count; i++)
            {
                m_Transforms[i].rotation = m_Rotations[i];
            }
        }
        else
        {
            for (int i = 0; i < m_Transforms.Count; i++)
            {
                test();//为了能更好说明效率这里加段耗时操作
                 m_Transforms[i].rotation = quaternion.AxisAngle(
                                   math.up(),
                                     (Time.frameCount % 360) * Mathf.Deg2Rad
                               );
            }
        }
        Debug.Log(((Time.realtimeSinceStartup - startTime) * 1000) + "ms");//统计耗时
    }



    [BurstCompile]
    struct MyJob : IJobParallelFor
    {
       [WriteOnly]public NativeArray<quaternion> rotations;
       [ReadOnly] public int frameCount;
        public void Execute(int index)
        {
            test();//为了能更好说明效率这里加段耗时操作
            rotations[index] = quaternion.AxisAngle(
                               math.up(),
                                 (frameCount % 360) * Mathf.Deg2Rad
                           );
        }
    }

    static void test()
    {
        float a = 0f;
        for (int i = 0; i < 10000; i++)
        {
            a = math.sqrt(i);
        }
    }


    /*
    class MySystem : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            float startTime = Time.realtimeSinceStartup;
            float a = UnityEngine.Time.frameCount;
            this.Entities.WithAll<Example12Alias>()
                   .ForEach((Transform rotation) =>
                   {

                       rotation.rotation = quaternion.AxisAngle(
                                          math.up(),
                                            (a % 360) * Mathf.Deg2Rad
                                      );
                   })
                   .WithoutBurst()
                   .Run();
            Debug.Log(((Time.realtimeSinceStartup - startTime) * 1000) + "ms");
            return inputDeps;
        }
    }
    */


    private void OnDestroy()
    {
        m_Rotations.Dispose();
    }
}
