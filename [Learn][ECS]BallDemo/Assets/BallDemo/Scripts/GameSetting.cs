﻿using UnityEngine;

namespace BallDemo
{
    public class GameSetting : MonoBehaviour
    {
        //生成的球的数量
        public int BallCount = 100000;
        //生成的球体的位置的范围
        public int BallBound = 50;
        //球的材质
        public Material BallMaterial;
        //球的mesh
        public Mesh BallMesh;

        public static GameSetting Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }
    }
}