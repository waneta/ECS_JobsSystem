﻿using System;
using Unity.Entities;

/// <summary>
/// 方块自身速度
/// </summary>
[Serializable]
public struct MoveSpeed : IComponentData
{
    public float speed;
}
public class MoveSpeedComponent : ComponentDataWrapper<RotationSpeed> { }
