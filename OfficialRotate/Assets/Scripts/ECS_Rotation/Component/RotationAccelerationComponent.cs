﻿using System;
using Unity.Entities;
/// <summary>
/// 方块的加速度 -1 速度逐渐变慢
/// </summary>
[Serializable]
public struct RotationAcceleration : IComponentData
{
    public float speed;
}
public class RotationAccelerationComponent : ComponentDataWrapper<RotationAcceleration> { }

