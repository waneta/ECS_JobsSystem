﻿using System;
using Unity.Entities;

/// <summary>
/// 方块自身速度
/// </summary>
[Serializable]
public struct RotationSpeed : IComponentData
{
    public float Value;
}
public class RotationSpeedComponent : ComponentDataWrapper<RotationSpeed> { }
