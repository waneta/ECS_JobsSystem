﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

//产卵器系统相关逻辑
public class SpawnRandomCircleSystem : ComponentSystem
{
    //对应产卵器的组件集合
    struct Group
    {
        //含有产卵所需的 个数、半径、预制体数据
        [ReadOnly] public SharedComponentDataArray<SpawnRandomCircle> Spawner;
        //产卵器位置数据
        public ComponentDataArray<Position> Position;
        //产卵器对应的 GameObject Entity 实体
        public EntityArray Entity;
        //因为目前产卵器只有一个，所以其 Length 数值为 1
        public readonly int Length;
    }
    //注入组件集合
    [Inject] Group m_Group;


    protected override void OnUpdate()
    {

        while (m_Group.Length != 0)
        {
            var spawner = m_Group.Spawner[0];
            var sourceEntity = m_Group.Entity[0];
            var center = m_Group.Position[0].Value;

            //根据产卵的个数声明对应个数的 entities 数组
            var entities = new NativeArray<Entity>(spawner.count, Allocator.Temp);
            //实例化cube
            EntityManager.Instantiate(spawner.prefab, entities);
            //创建对应的position数组（个数等于cube创建个数）
            var positions = new NativeArray<float3>(spawner.count, Allocator.Temp);

            if (spawner.spawnLocal)
            {
                //计算出每一个Cube对应的Position位置 使用 ref 填充
                GeneratePoints.RandomPointsOnCircle(new float3(), spawner.radius, ref positions);

                //遍历Position赋值
                for (int i = 0; i < spawner.count; i++)
                {
                    var position = new LocalPosition
                    {
                        Value = positions[i]
                    };
                    //为每一个Entity赋值
                    EntityManager.SetComponentData(entities[i], position);
                    //因为选择的是spawnLocal，所以要为对应的 entity添加 TransformParent（类似于原来的 transform.SetParent）
                    EntityManager.AddComponentData(entities[i], new TransformParent { Value = sourceEntity });
                }
            }
            else
            {
                GeneratePoints.RandomPointsOnCircle(center, spawner.radius, ref positions);
                for (int i = 0; i < spawner.count; i++)
                {
                    var position = new Position
                    {
                        Value = positions[i]
                    };
                    EntityManager.SetComponentData(entities[i], position);
                }
            }

            entities.Dispose();
            positions.Dispose();

            EntityManager.RemoveComponent<SpawnRandomCircle>(sourceEntity);

            //实例化 & AddComponent和RemoveComponent调用使注入的组无效，
            //所以在我们进入下一个产卵之前我们必须重新注入它们
            UpdateInjectedComponentGroups();
        }
    }
}

