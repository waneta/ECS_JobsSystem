﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Transforms;

//在RotationSpeedSystem前运行
[UpdateBefore(typeof(RotationSpeedSystem))]
public class RotationSpeedResetSphereSystem : JobComponentSystem
{
    /// <summary>
    /// Logo对应的entity group
    /// </summary>
    struct RotationSpeedResetSphereGroup
    {
        //Logo给予Cube速度对应的数据
        [ReadOnly] public ComponentDataArray<RotationSpeedResetSphere> rotationSpeedResetSpheres;
        //Logo对应的旋转半径
        [ReadOnly] public ComponentDataArray<Radius> spheres;
        //Logo对应的位置
        [ReadOnly] public ComponentDataArray<Position> positions;
        public readonly int Length;
    }
    //注入Logo组件集合
    [Inject] RotationSpeedResetSphereGroup m_RotationSpeedResetSphereGroup;

    /// <summary>
    /// 方块的entity group
    /// </summary>
    struct RotationSpeedGroup
    {
        //方块自身的旋转速度
        public ComponentDataArray<RotationSpeed> rotationSpeeds;
        //方块的位置
        [ReadOnly] public ComponentDataArray<Position> positions;
        //固定写法 数值等于Cube的个数
        public readonly int Length;
    }
    //注入Cube组件集合
    [Inject] RotationSpeedGroup m_RotationSpeedGroup;

    [BurstCompile]
    struct RotationSpeedResetSphereRotation : IJobParallelFor
    {
        /// <summary>
        /// 方块的速度
        /// </summary>
        public ComponentDataArray<RotationSpeed> rotationSpeeds;
        /// <summary>
        /// 方块的坐标
        /// </summary>
        [ReadOnly] public ComponentDataArray<Position> positions;

        //下面都是Logo上面的组件
        [ReadOnly] public ComponentDataArray<RotationSpeedResetSphere> rotationSpeedResetSpheres;
        [ReadOnly] public ComponentDataArray<Radius> spheres;
        [ReadOnly] public ComponentDataArray<Position> rotationSpeedResetSpherePositions;

        public void Execute(int i)//i 0-9  这个i值取对应 Schedule 中设置的 arrayLength 的数值 此Code中设置的为 m_RotationSpeedGroup.Length
        {
            //UnityEngine.Debug.Log($"长度{i}");
            //方块的中心点
            var center = positions[i].Value;

            for (int positionIndex = 0; positionIndex < rotationSpeedResetSpheres.Length; positionIndex++)
            {
                //计算圆球与方块的距离 ，小于指定具体传入速度
                if (math.distance(rotationSpeedResetSpherePositions[positionIndex].Value, center) < spheres[positionIndex].radius)
                {
                    rotationSpeeds[i] = new RotationSpeed
                    {
                        Value = rotationSpeedResetSpheres[positionIndex].speed
                    };
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var rotationSpeedResetSphereRotationJob = new RotationSpeedResetSphereRotation
        {
            rotationSpeedResetSpheres = m_RotationSpeedResetSphereGroup.rotationSpeedResetSpheres,
            spheres = m_RotationSpeedResetSphereGroup.spheres,
            rotationSpeeds = m_RotationSpeedGroup.rotationSpeeds,
            rotationSpeedResetSpherePositions = m_RotationSpeedResetSphereGroup.positions,
            positions = m_RotationSpeedGroup.positions
        };

        return rotationSpeedResetSphereRotationJob.Schedule(m_RotationSpeedGroup.Length, 32, inputDeps);
    }
}

