﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using UnityEngine;

public class RotationAccelerationSystem : JobComponentSystem
{
    [BurstCompile]
    struct RotationSpeedAcceleration : IJobProcessComponentData<RotationSpeed, RotationAcceleration>
    {
        public float dt;
        //对Cube自身的RotationSpeed进行衰减处理
        public void Execute(ref RotationSpeed speed, [ReadOnly]ref RotationAcceleration acceleration)
        {
            speed.Value = math.max(0.0f, speed.Value + (acceleration.speed * dt));
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var rotationSpeedAccelerationJob = new RotationSpeedAcceleration { dt = Time.deltaTime };
        //return rotationSpeedAccelerationJob.Schedule(this, 64, inputDeps);
        return rotationSpeedAccelerationJob.Schedule(this, inputDeps);
    }
}