﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


//Logo运动相关逻辑
public class MoveAlongCircleSystem : JobComponentSystem
{
    // Logo运动相关逻辑中需要用到的数据
    struct MoveAlongCircleGroup
    {
        //Logo位置
        public ComponentDataArray<Position> positions;
        //旋转的中心点和半径数据
        public ComponentDataArray<MoveAlongCircle> moveAlongCircles;
        //旋转速度数据
        [ReadOnly] public ComponentDataArray<MoveSpeed> moveSpeeds;
        //固定写法
        public readonly int Length;
    }
    //注入数据 Inject自带特性
    [Inject] private MoveAlongCircleGroup m_MoveAlongCircleGroup;


    [BurstCompile]
    struct MoveAlongCirclePosition : IJobParallelFor//Logo位置旋转更新逻辑，可以理解为传统模式中的Update
    {
        /// <summary>
        /// 位置数据
        /// </summary>
        public ComponentDataArray<Position> positions;
        /// <summary>
        /// 中心点及半径数据
        /// </summary>
        public ComponentDataArray<MoveAlongCircle> moveAlongCircles;
        /// <summary>
        /// 运行速度
        /// </summary>
        [ReadOnly] public ComponentDataArray<MoveSpeed> moveSpeeds;
        /// <summary>
        /// 运行时间
        /// </summary>
        public float dt;

        /// <summary>
        /// 并行执行for循环 i 根据length计算 打印的一直是0
        /// </summary>
        /// <param name="i"></param>
        public void Execute(int i)
        {
            //Debug.Log(i); //打印的一直是0 虽然可以打印，但是会报错，希望官方会出针对 ECS 的 Debug.Log

            //运行时间
            float t = moveAlongCircles[i].t + (dt * moveSpeeds[i].speed);
            //位置偏移量
            float offsetT = t + (0.01f * i);
            float x = moveAlongCircles[i].center.x + (math.cos(offsetT) * moveAlongCircles[i].radius);
            float y = moveAlongCircles[i].center.y;
            float z = moveAlongCircles[i].center.z + (math.sin(offsetT) * moveAlongCircles[i].radius);

            moveAlongCircles[i] = new MoveAlongCircle
            {
                t = t,
                center = moveAlongCircles[i].center,
                radius = moveAlongCircles[i].radius
            };
            //更新Logo的位置
            positions[i] = new Position
            {
                Value = new float3(x, y, z)
            };
        }
    }

    //数据初始化
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var moveAlongCirclePositionJob = new MoveAlongCirclePosition();
        moveAlongCirclePositionJob.positions = m_MoveAlongCircleGroup.positions;
        moveAlongCirclePositionJob.moveAlongCircles = m_MoveAlongCircleGroup.moveAlongCircles;
        moveAlongCirclePositionJob.moveSpeeds = m_MoveAlongCircleGroup.moveSpeeds;
        moveAlongCirclePositionJob.dt = Time.deltaTime;
        return moveAlongCirclePositionJob.Schedule(m_MoveAlongCircleGroup.Length, 64, inputDeps);
    }
}

